<?php

define('ROOT', rtrim(dirname(__FILE__), '/') . '/../private/');
define('LIBROOT', ROOT . 'Lib/');
define('CONFROOT', ROOT . 'Conf/');

require_once LIBROOT . 'Core/Autoloader.php';
require_once ROOT . '/../vendor/autoload.php';
