#!/usr/bin/env php
<?php

require_once __DIR__ . DIRECTORY_SEPARATOR . 'bootstrap.php';

$number = $argv[1] ?? 0;
$hostname = \Lib\Core\Settings::getInstance()->get(
    ['ws', 'servers', $number]
);
$server = new \Lib\Server\Server($hostname);

declare(ticks = 1);

register_shutdown_function([$server, 'close']);

\Lib\Core\Settings::getInstance()->set('currentServer', $hostname);

try {
    $server->run();
} catch (\Throwable $exception) {
    $server->close();
    print_r($exception);
}