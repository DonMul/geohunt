<?php

namespace Controller;

/**
 * Class Index
 * @package Controller
 */
final class Index extends \Lib\Core\BaseController
{
    /**
     * @return array
     */
    public function getArray(): array
    {
        return [];
    }
}
