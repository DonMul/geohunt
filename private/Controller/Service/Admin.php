<?php

namespace Controller\Service;

use Lib\Exception\UnauthorizedException;

/**
 * Class Admin
 * @package Controller\Service
 */
final class Admin extends \Lib\Core\BaseController\Ajax
{
    /**
     * @return array
     * @throws UnauthorizedException
     */
    public function getArray(): array
    {
        if (!isset($_SESSION['authed'])) {
            throw new UnauthorizedException();
        }

        return [];
    }
}
