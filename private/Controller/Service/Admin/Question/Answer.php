<?php

namespace Controller\Service\Admin\Question;

use Lib\Server\Client;
use Lib\Server\Command\Admin\Answer\Correct;
use Lib\Server\Command\Admin\Answer\Remove;
use Lib\Server\Command\Admin\Answer\Wrong;

/**
 * Class Answer
 * @package Controller
 * @author Joost Mul <scoutingcms@jmul.net>
 */
final class Answer extends \Lib\Core\BaseController\Ajax
{
    const TYPE_CORRECT = 'correct';
    const TYPE_WRONG = 'wrong';
    const TYPE_REMOVE = 'removeAnswer';

    /**
     * @return array
     *
     * @throws \Lib\Exception\Server\IoException
     */
    public function getArray(): array
    {
        $action = $this->getPostValue('action', 'wrong');

        $client = Client::getInstance();
        $payload = [
            'teamId' => $this->getPostValue('teamId', 0),
            'questionId' => $this->getPostValue('questionId', 0)
        ];

        switch ($action) {
            case self::TYPE_CORRECT:
                $client->writeCommand(Correct::COMMAND, $payload);
                break;
            case self::TYPE_WRONG:
                $client->writeCommand(Wrong::COMMAND, $payload);
                break;
            case self::TYPE_REMOVE:
                $client->writeCommand(Remove::COMMAND, $payload);
                break;
        }

        return [
            'reload' => true,
        ];
    }
}
