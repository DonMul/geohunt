<?php

namespace Controller\Service\Admin;

use Lib\Core\Translation;

/**
 * Class Auth
 * @package Controller\Service\Admin
 */
final class Auth extends \Lib\Core\BaseController\Ajax
{
    /**
     * @return array
     */
    public function getArray(): array
    {
        $authCode = $this->getPostValue('code', '');

        $isSuccess = false;
        if ($authCode == 'flg') {
            $isSuccess = true;
            $_SESSION['authed'] = true;
        } else {
        }

        return [
            'message' => Translation::getInstance()->translate('common.loggedIn'),
            'reload' => true,
        ];
    }
}
