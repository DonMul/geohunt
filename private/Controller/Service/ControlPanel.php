<?php

namespace Controller\Service;

use Lib\Core\Session;
use Lib\Exception\UnauthorizedException;

/**
 * Class ControlPanel
 * @package Controller
 */
abstract class ControlPanel extends \Lib\Core\BaseController\Ajax
{
    /**
     * @throws UnauthorizedException
     */
    protected function ensureLoggedIn(): void
    {
        if (Session::getInstance()->isLoggedIn() === false) {
            throw new UnauthorizedException();
        }
    }
}
