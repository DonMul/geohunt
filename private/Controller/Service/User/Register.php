<?php

namespace Controller\Service\User;

use Lib\Core\Session;
use Lib\Core\Translation;
use Lib\Core\Validate;
use Lib\Data\User;
use Lib\Exception\User\PasswordsNotMatchingException;
use Lib\Exception\User\UsernameAlreadyTakenException;
use Lib\Exception\UserException;
use Lib\Exception\Validate\InvalidEmailAddressException;
use Lib\Exception\Validate\InvalidPasswordException;

/**
 * Class Login
 * @package Controller\Service\User
 */
final class Register extends \Lib\Core\BaseController\Ajax
{
    /**
     * @return array
     * @throws UserException
     */
    public function getArray(): array
    {
        $username = $this->getPostValue('username', '');
        $password = $this->getPostValue('password', '');
        $passwordRepeat = $this->getPostValue('passwordRepeat', '');
        $emailAddress = $this->getPostValue('email', '');

        $user = $this->getUserRepo()->getByUsername($username);
        if ($user instanceof User) {
            throw new UsernameAlreadyTakenException($username);
        }

        if (!Validate::getInstance()->isValidPassword($password)) {
            throw new InvalidPasswordException();
        }

        if ($password !== $passwordRepeat) {
            throw new PasswordsNotMatchingException();
        }

        if (!Validate::getInstance()->isValidEmailAddress($emailAddress)) {
            throw new InvalidEmailAddressException($emailAddress);
        }

        $user = new User(
            0,
            $username,
            User::hashPassword($password),
            $emailAddress,
            User::ROLE_USER
        );

        $this->getUserRepo()->save($user);

        Session::getInstance()->logIn($user->getId());

        return [
            'message' => Translation::getInstance()->translate('common.registered'),
            'redirect' => Translation::getInstance()->translateLink('cp'),
        ];
    }
}
