<?php

namespace Controller\Service\User;

use Lib\Core\Session;
use Lib\Core\Translation;
use Lib\Data\User;
use Lib\Exception\User\InvalidUsernameOrPasswordException;
use Lib\Exception\UserException;

/**
 * Class Login
 * @package Controller\Service\User
 */
final class Login extends \Lib\Core\BaseController\Ajax
{
    /**
     * @return array
     * @throws UserException
     */
    public function getArray(): array
    {
        $password = $this->getPostValue('password', '');
        $username = $this->getPostValue('username', '');

        $user = $this->getUserRepo()->getByUsername($username);

        if (!($user instanceof User) || $user->getPassword() != User::hashPassword($password)) {
            throw new InvalidUsernameOrPasswordException();
        }

        Session::getInstance()->logIn($user->getId());

        return [
            'message' => Translation::getInstance()->translate('common.loggedIn'),
            'reload' => true,
        ];
    }
}
