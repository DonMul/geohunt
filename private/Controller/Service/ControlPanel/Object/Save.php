<?php

namespace Controller\Service\ControlPanel\Object;

use Lib\Core\Session;
use Lib\Core\Translation;
use Lib\Core\Validate;
use Lib\Data\Answer;
use Lib\Data\BuyableObject;
use Lib\Data\Game;
use Lib\Data\Question;
use Lib\Exception\UnauthorizedException;
use Lib\Exception\UserException;
use Lib\Exception\Validate\MaxValueException;
use Lib\Exception\Validate\MinLengthException;
use Lib\Exception\Validate\MinValueException;

/**
 * Class Save
 * @package Controller\Service\ControlPanel\Game
 */
final class Save extends \Controller\Service\ControlPanel
{
    public function validate()
    {
        $name = $this->getPostValue('name', '');

        $price = intval($this->getPostValue('price', -1));
        $points = intval($this->getPostValue('points', -1));

        $errors = [];
        if (Validate::getInstance()->isMinLength($name, 3) === false) {
            $errors[] = new MinLengthException(Translation::getInstance()->translate('object.name'), 3);
        }

        if (Validate::getInstance()->isMinValue($price, 0) === false) {
            $errors[] = new MinValueException(Translation::getInstance()->translate('object.price'), 0);
        }

        if (Validate::getInstance()->isMinValue($points, 0) === false) {
            $errors[] = new MinValueException(Translation::getInstance()->translate('object.points'), 0);
        }

        foreach ($errors as $error) {
            $this->errors[] = $error->getMessage();
        }
    }

    /**
     * @return array
     * @throws MinLengthException
     * @throws \Lib\Exception\UserException
     */
    public function getArray(): array
    {
        $gameId = $this->getPostValue('gameId', 0);
        $objectId = $this->getPostValue('objectId', 0);

        $name = $this->getPostValue('name', '');
        $price = intval($this->getPostValue('price', 0));
        $points = intval($this->getPostValue('points', 0));

        $game = $this->getGameRepo()->getByIdAndUserId($gameId, Session::getInstance()->getKey());
        if (!($game instanceof Game)) {
            throw new UnauthorizedException();
        }

        $object = new BuyableObject(
            0,
            $name,
            '',
            $price,
            $points,
            $game->getId()
        );

        if ($objectId != 0) {
            $object = $this->getObjectRepo()->getByIdGameIdAndUserId($objectId, $game->getId(), Session::getInstance()->getKey());

            if (!($object instanceof BuyableObject)) {
                throw new UnauthorizedException();
            }

            $object->setName($name);
            $object->setPurchaseValue($price);
            $object->setPoints($points);
        }

        $this->getObjectRepo()->save($object);

        if ($_FILES['image']) {
            $targetDir = realpath(ROOT . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR . $game->getId();

            if (!is_dir($targetDir)) {
                $isCreated = mkdir($targetDir, 0777, true);
                if ($isCreated === false) {
                    throw new UserException("Could not create dir {$targetDir}");
                }
            }

            $imageFileType = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
            $targetFile = $targetDir . DIRECTORY_SEPARATOR . $object->getId() . '.' . $imageFileType;

            $allowedTypes = ['jpg', 'png', 'jpeg', 'gif'];
            if (in_array($imageFileType, $allowedTypes) === false) {
                throw new UserException(Translation::getInstance()->translate('error.invalidUploadImageType', ['types' => implode(', ', $allowedTypes)]));
            }

            if (file_exists($targetFile)) {
                unlink($targetFile);
            }

            $isSuccess = move_uploaded_file($_FILES["image"]["tmp_name"], $targetFile);

            if (!$isSuccess) {
                throw new UserException(Translation::getInstance()->translate('error.somethingWentWrongDuringUpload'));
            }

            $object->setImage(DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR . $game->getId() . DIRECTORY_SEPARATOR . $object->getId() . '.' . $imageFileType);
            $this->getObjectRepo()->save($object);
        }

        return [
            'redirect' => Translation::getInstance()->translateLink('cpGameObjects', ['gameId' => $game->getId()])
        ];
    }
}
