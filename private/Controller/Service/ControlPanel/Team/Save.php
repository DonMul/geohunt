<?php

namespace Controller\Service\ControlPanel\Team;

use Lib\Core\Session;
use Lib\Core\Translation;
use Lib\Core\Validate;
use Lib\Data\Game;
use Lib\Data\Team;
use Lib\Exception\UnauthorizedException;
use Lib\Exception\Validate\MinLengthException;

/**
 * Class Save
 * @package Controller\Service\ControlPanel\Game
 */
final class Save extends \Controller\Service\ControlPanel
{
    /**
     * @return array
     * @throws MinLengthException
     * @throws \Lib\Exception\UserException
     */
    public function getArray(): array
    {
        $this->ensureLoggedIn();

        $name = $this->getPostValue('name', '');
        $code = $this->getPostValue('code', '');
        $points = $this->getPostValue('points', 0);
        $teamId = $this->getPostValue('teamId', 0);

        $game = $this->getGameRepo()->getByIdAndUserId($this->getPostValue('gameId'), Session::getInstance()->getKey());
        if (!($game instanceof Game)) {
            throw new UnauthorizedException();
        }

        if (!Validate::getInstance()->isMinLength($name, 3)) {
            throw new MinLengthException(Translation::getInstance()->translate('team.name'), 3);
        }

        if (empty($code)) {
            $code = uniqid();
        }

        $team = new Team(
            0,
            $name,
            $code,
            $points,
            0,
            0,
            $game->getId()
        );

        if ($teamId != 0) {
            $team = $this->getTeamRepo()->getByIdGameIdAndUserId($teamId, $game->getId(), Session::getInstance()->getKey());

            if (!($team instanceof Team)) {
                throw new UnauthorizedException();
            }

            $team->setName($name);
            $team->setPoints($points);
            $team->setCode($code);
        }

        $this->getTeamRepo()->save($team);

        return [
            'redirect' => Translation::getInstance()->translateLink('cpGameTeams', ['gameId' => $game->getId()]),
        ];
    }
}
