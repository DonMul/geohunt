<?php

namespace Controller\Service\ControlPanel\Game;

use Lib\Core\Session;
use Lib\Core\Translation;
use Lib\Core\Validate;
use Lib\Data\Game;
use Lib\Exception\Validate\MinLengthException;
use Lib\Exception\Validate\ValidDateInFutureException;

/**
 * Class NewGame
 * @package Controller\Service\ControlPanel\Game
 */
final class NewGame extends \Controller\Service\ControlPanel
{
    /**
     * @return array
     * @throws MinLengthException
     * @throws \Lib\Exception\UserException
     */
    public function getArray(): array
    {
        $this->ensureLoggedIn();

        $name = $this->getPostValue('name', '');
        $startDate = $this->getPostValue('startDate', '');
        $endDate = $this->getPostValue('endDate', '');

        if (!Validate::getInstance()->isMinLength($name, 3)) {
            throw new MinLengthException(Translation::getInstance()->translate('game.name'), 3);
        }

        if (!Validate::getInstance()->isValidDateInFuture($startDate)) {
            throw new ValidDateInFutureException(Translation::getInstance()->translate('game.startDate'));
        }

        if (!Validate::getInstance()->isValidDateInFuture($endDate)) {
            throw new ValidDateInFutureException(Translation::getInstance()->translate('game.startDate'));
        }

        $game = new Game(
            0,
            Session::getInstance()->getKey(),
            $name,
            date('Y-m-d H:i:s', strtotime($startDate)),
            date('Y-m-d H:i:s', strtotime($endDate))
        );

        $this->getGameRepo()->save($game);

        return [
            'redirect' => Translation::getInstance()->translateLink('cpGame', ['id' => $game->getId()]),
        ];
    }
}
