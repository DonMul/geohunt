<?php

namespace Controller\Service\ControlPanel\Question;

use Lib\Core\Session;
use Lib\Core\Translation;
use Lib\Core\Validate;
use Lib\Data\Answer;
use Lib\Data\Game;
use Lib\Data\Question;
use Lib\Exception\UnauthorizedException;
use Lib\Exception\UserException;
use Lib\Exception\Validate\MaxValueException;
use Lib\Exception\Validate\MinLengthException;
use Lib\Exception\Validate\MinValueException;

/**
 * Class Save
 * @package Controller\Service\ControlPanel\Game
 */
final class Save extends \Controller\Service\ControlPanel
{
    public function validate()
    {
        $title = $this->getPostValue('title', '');

        $value = $this->getPostValue('value', 0);
        $lat = $this->getPostValue('lat', 0);
        $lon = $this->getPostValue('lon', 0);
        $type = $this->getPostValue('type', '');
        $answers = $this->getPostValue('answer', []);

        $errors = [];
        if (Validate::getInstance()->isMinLength($title, 3) === false) {
            $errors[] = new MinLengthException(Translation::getInstance()->translate('question.title'), 3);
        }

        if (Validate::getInstance()->isMinValue($value, 0) === false) {
            $errors[] = new MinValueException(Translation::getInstance()->translate('question.value'), 0);
        }

        if (Validate::getInstance()->isMinValue($lat, -180) === false) {
            $errors[] = new MinValueException(Translation::getInstance()->translate('question.lat'), -180);
        }

        if (Validate::getInstance()->isMinValue($lon, -90) === false) {
            $errors[] = new MinValueException(Translation::getInstance()->translate('question.lon'), -90);
        }

        if (Validate::getInstance()->isMaxValue($lat, 180) === false) {
            $errors[] = new MaxValueException(Translation::getInstance()->translate('question.lat'), 180);
        }

        if (Validate::getInstance()->isMaxValue($lon, 90) === false) {
            $errors[] = new MaxValueException(Translation::getInstance()->translate('question.lon'), 90);
        }

        if (!in_array($type, Question::getTypes())) {
            $errors[] = new UserException(Translation::getInstance()->translate('error.question.invalidType'));
        }

        if ($type == Question::TYPE_MULTIPLE && count($answers) <= 0) {
            $errors[] = new UserException(Translation::getInstance()->translate('error.question.answersRequired'));
        }

        foreach ($errors as $error) {
            $this->errors[] = $error->getMessage();
        }
    }

    /**
     * @return array
     * @throws MinLengthException
     * @throws \Lib\Exception\UserException
     */
    public function getArray(): array
    {
        $gameId = $this->getPostValue('gameId', 0);
        $questionId = $this->getPostValue('questionId', 0);
        $title = $this->getPostValue('title', '');
        $description = $this->getPostValue('description', '');
        $value = $this->getPostValue('value', 0);
        $lat = $this->getPostValue('lat', 0);
        $lon = $this->getPostValue('lon', 0);
        $type = $this->getPostValue('type', '');
        $answers = $this->getPostValue('answer', []);

        $game = $this->getGameRepo()->getByIdAndUserId($gameId, Session::getInstance()->getKey());
        if (!($game instanceof Game)) {
            throw new UnauthorizedException();
        }

        $question = new Question(
            0,
            $title,
            $description,
            $value,
            $lat,
            $lon,
            $type,
            $game->getId()
        );

        if ($questionId != 0) {
            $question = $this->getQuestionRepo()->getByIdGameIdAndUserId($questionId, $game->getId(), Session::getInstance()->getKey());

            if (!($question instanceof Question)) {
                throw new UnauthorizedException();
            }

            $question->setTitle($title);
            $question->setDescription($description);
            $question->setValue($value);
            $question->setLon($lon);
            $question->setLat($lat);
            $question->setType($type);
        }

        $this->getQuestionRepo()->save($question);

        $currentAnswers = $this->getAnswerRepo()->findByQuestionId($question->getId());
        $newQuqestionIds = [];
        if ($question->getType() == Question::TYPE_MULTIPLE) {
            $i = 0;
            foreach ($answers as $id => $data) {
                $answer = null;
                if (is_numeric($id)) {
                    $answer = $this->getAnswerRepo()->getByIdAndQuestionId($id, $question->getId());
                }

                if ($answer instanceof Answer) {
                    $answer->setAnswer($data['answer']);
                    $answer->setIsCorrect(isset($data['correct']) ? boolval($data['correct']) : false);
                    $answer->setPosition($i++);
                } else {
                    $answer = new \Lib\Data\Answer(
                        0,
                        $question->getId(),
                        $i++,
                        $data['answer'],
                        isset($data['correct']) ? boolval($data['correct']) : false
                    );
                }

                $this->getAnswerRepo()->save($answer);
                $newQuqestionIds[] = $answer->getId();
            }
        }

        foreach ($currentAnswers as $currentAnswer) {
            if (in_array($currentAnswer->getId(), $newQuqestionIds)) {
                continue;
            }

            $this->getAnswerRepo()->delete($currentAnswer);
        }


        return [
            'redirect' => Translation::getInstance()->translateLink('cpGameQuestions', ['gameId' => $game->getId()])
        ];
    }
}
