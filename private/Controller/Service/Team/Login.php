<?php

namespace Controller\Service\Team;

use Lib\Core\Session;
use Lib\Core\Translation;
use Lib\Data\Team;
use Lib\Exception\UserException;

/**
 * Class Login
 * @package Controller\Service\User
 */
final class Login extends \Lib\Core\BaseController\Ajax
{
    /**
     * @return array
     * @throws UserException
     */
    public function getArray(): array
    {
        $authCode = $this->getPostValue('teamCode', '');
        $team = $this->getTeamRepo()->getByCode($authCode);

        if (!($team instanceof Team)) {
            throw new UserException(Translation::getInstance()->translate('error.invalidCode'));
        }

        Session::getInstance()->set('loggedInTeamId', $team->getId());

        return [
            'message' => Translation::getInstance()->translate('common.loggedIn'),
            'redirect' => Translation::getInstance()->translateLink('play'),
        ];
    }
}
