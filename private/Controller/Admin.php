<?php

namespace Controller;

/**
 * Class Admin
 * @package Controller
 */
class Admin extends \Lib\Core\BaseController
{
    /**
     * @return array
     */
    public function getArray(): array
    {
        if (!isset($_SESSION['authed'])) {
            $this->serveTemplate('Admin\\Auth.html.twig', []);
            exit;
        }

        return [];
    }
}
