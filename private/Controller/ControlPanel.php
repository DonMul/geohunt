<?php

namespace Controller;

use Lib\Core\Session;

/**
 * Class ControlPanel
 * @package Controller
 */
class ControlPanel extends \Lib\Core\BaseController
{
    /**
     *
     */
    protected function ensureLoggedIn(): void
    {
        if (Session::getInstance()->isLoggedIn() === false) {
            $this->serveTemplate('ControlPanel\\Auth.html.twig', []);
            exit;
        }
    }

    /**
     * @return array
     */
    public function getArray(): array
    {
        $this->ensureLoggedIn();

        return [
            'games' => $this->getGameRepo()->findByUserId(Session::getInstance()->getKey()),
            'gameAmount' => $this->getGameRepo()->getAmountForUser(Session::getInstance()->getKey()),
            'questionAmount' => $this->getQuestionRepo()->getAmountForUser(Session::getInstance()->getKey()),
            'teamAmount' => $this->getTeamRepo()->getAmountForUser(Session::getInstance()->getKey()),
            'objectAmount' => $this->getObjectRepo()->getAmountForUser(Session::getInstance()->getKey()),
        ];
    }
}
