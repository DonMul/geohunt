<?php

namespace Controller;

/**
 * Class FourOFour
 * @package Controller
 */
final class FourOFour extends \Lib\Core\BaseController
{
    /**
     * @return array
     */
    public function getArray(): array
    {
        return [];
    }
}
