<?php

namespace Controller;

use Lib\Core\Session;
use Lib\Exception\UnauthorizedException;

/**
 * Class Play
 * @package Controller
 */
final class Play extends \Lib\Core\BaseController
{
    /**
     * @return array
     * @throws UnauthorizedException
     */
    public function getArray(): array
    {
        $team = $this->getTeamRepo()->getById(Session::getInstance()->get('loggedInTeamId', 0));
        if (!($team instanceof \Lib\Data\Team)) {
            throw new UnauthorizedException();
        }

        return [
            'team' => $team,
            'questions' => $this->enrichQuestions(array_map(function (\Lib\Data\Question $question) {
                return $question->toArray();
            }, $this->getQuestionRepo()->getAll())),
            'objects' => $this->getObjectRepo()->getAllByTeam($team)
        ];
    }

    /**
     * @param array $questions
     * @return array
     */
    private function enrichQuestions(array $questions): array
    {
        foreach ($questions as &$question) {
            $answers = [];
            if ($question['type'] == 'multiple') {
                $answers = array_map(function (\Lib\Data\Answer $answer) {
                    return $answer->toArray();
                }, $this->getAnswerRepo()->findByQuestionId($question['id']));
            }

            $question['answers'] = $answers;
        }

        return $questions;
    }
}
