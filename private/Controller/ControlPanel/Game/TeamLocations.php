<?php

namespace Controller\ControlPanel\Game;

use Lib\Core\Session;
use Lib\Data\Game;
use Lib\Exception\UnauthorizedException;

/**
 * Class TeamLocations
 * @package Controller\ControlPanel\Game
 */
final class TeamLocations extends \Controller\ControlPanel
{
    /**
     * @return array
     * @throws UnauthorizedException
     */
    public function getArray(): array
    {
        $this->ensureLoggedIn();

        $gameId = $this->getPostValue('gameId', 0);
        $game = $this->getGameRepo()->getByIdAndUserId($gameId, Session::getInstance()->getKey());
        if (!($game instanceof Game)) {
            throw new UnauthorizedException();
        }

        $teams = $this->getTeamRepo()->findByGameId($game->getId());

        $locations = [];
        foreach ($teams as $team) {
            $locations[$team->getId()] = [
                'lat' => $team->getLat(),
                'lon' => $team->getLon(),
            ];
        }

        return [
            'locations' => $locations,
        ];
    }
}
