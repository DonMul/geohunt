<?php

namespace Controller\ControlPanel\Game;

use Lib\Core\Session;
use Lib\Data\Game;
use Lib\Exception\UnauthorizedException;

/**
 * Class Answers
 * @package Controller\ControlPanel\Game
 */
final class Answers extends \Controller\ControlPanel
{
    /**
     * @return array
     * @throws UnauthorizedException
     */
    public function getArray(): array
    {
        $this->ensureLoggedIn();

        $game = $this->getGameRepo()->getByIdAndUserId($this->getVariable('gameId'), Session::getInstance()->getKey());
        if (!($game instanceof Game)) {
            throw new UnauthorizedException();
        }

        $answers = $this->getAnswerRepo()->getAnsweredInfoForGame($game);
        $teamSorted = [];
        foreach ($answers as $answer) {
            $teamSorted[$answer['tId']][] = $answer;
        }

        return [
            'game' => $game,
            'teams' => $this->getTeamRepo()->findByGameId($game->getId()),
        ];
    }
}
