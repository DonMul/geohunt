<?php

namespace Controller\ControlPanel\Game\Question;

use Lib\Core\Session;
use Lib\Data\Game;
use Lib\Data\Question;
use Lib\Exception\UnauthorizedException;

/**
 * Class Edit
 * @package Controller\ControlPanel\Game\Team
 */
final class Edit extends \Controller\ControlPanel
{
    /**
     * @return array
     * @throws UnauthorizedException
     */
    public function getArray(): array
    {
        $this->ensureLoggedIn();

        $gameId = $this->getVariable('gameId');
        $questionId = $this->getVariable('questionId');

        $game = $this->getGameRepo()->getByIdAndUserId($gameId, Session::getInstance()->getKey());
        if (!($game instanceof Game)) {
            throw new UnauthorizedException();
        }

        $question = new Question(0, '', '', 0, 0, 0, Question::TYPE_MULTIPLE, $game->getId());

        if ($questionId != 0) {
            $question = $this->getQuestionRepo()->getByIdGameIdAndUserId($questionId, $game->getId(), Session::getInstance()->getKey());

            if (!($question instanceof Question)) {
                throw new UnauthorizedException();
            }
        }

        return array_merge(parent::getArray(), [
            'game' => $game,
            'question' => $question,
        ]);
    }
}
