<?php

namespace Controller\ControlPanel\Game;

use Lib\Core\Session;
use Lib\Data\Game;
use Lib\Exception\UnauthorizedException;

/**
 * Class Overview
 * @package Controller\ControlPanel\Game
 */
final class Overview extends \Controller\ControlPanel
{
    /**
     * @return array
     * @throws UnauthorizedException
     */
    public function getArray(): array
    {
        $this->ensureLoggedIn();

        $game = $this->getGameRepo()->getByIdAndUserId($this->getVariable('gameId'), Session::getInstance()->getKey());
        if (!($game instanceof Game)) {
            throw new UnauthorizedException();
        }

        $teams = $this->getTeamRepo()->findByGameId($game->getId());
        $colors = [];
        foreach ($teams as $team) {
            $hash = md5($team->getId());
            if (mb_strlen($team) >= 3) {
                $hash = $team->getName();
            }

            $colors[$team->getId()] = [
                'r' => (ord($hash{0}) * ord($hash{1})) % 255,
                'g' => (ord($hash{2}) * ord($hash{3})) % 255,
                'b' => (ord($hash{4}) * ord($hash{5})) % 255
            ];
        }

        return array_merge(parent::getArray(), [
            'questions' => $this->getQuestionRepo()->findByGameId($game->getId()),
            'objects' => $this->getObjectRepo()->findByGameId($game->getId()),
            'game' => $game,
            'teams' => $teams,
            'teamColors' => $colors,
        ]);
    }
}
