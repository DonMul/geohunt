<?php

namespace Controller\ControlPanel\Game\Object;

use Lib\Core\Session;
use Lib\Data\BuyableObject;
use Lib\Data\Game;
use Lib\Exception\UnauthorizedException;

/**
 * Class Edit
 * @package Controller\ControlPanel\Game\Object
 */
final class Edit extends \Controller\ControlPanel
{
    /**
     * @return array
     * @throws UnauthorizedException
     */
    public function getArray(): array
    {
        $this->ensureLoggedIn();

        $gameId = $this->getVariable('gameId');
        $objectId = $this->getVariable('objectId');

        $game = $this->getGameRepo()->getByIdAndUserId($gameId, Session::getInstance()->getKey());
        if (!($game instanceof Game)) {
            throw new UnauthorizedException();
        }

        $object = new BuyableObject(0, '', '', 0, 0, $game->getId());

        if ($objectId != 0) {
            $object = $this->getObjectRepo()->getByIdGameIdAndUserId($objectId, $game->getId(), Session::getInstance()->getKey());

            if (!($object instanceof BuyableObject)) {
                throw new UnauthorizedException();
            }
        }

        return array_merge(parent::getArray(), [
            'game' => $game,
            'object' => $object,
        ]);
    }
}
