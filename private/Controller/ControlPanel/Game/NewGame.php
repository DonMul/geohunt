<?php

namespace Controller\ControlPanel\Game;

use Lib\Exception\UnauthorizedException;

/**
 * Class NewGame
 * @package Controller\ControlPanel
 */
final class NewGame extends \Controller\ControlPanel
{
    /**
     * @return array
     * @throws UnauthorizedException
     */
    public function getArray(): array
    {
        $this->ensureLoggedIn();

        return array_merge(parent::getArray(), []);
    }
}
