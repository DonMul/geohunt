<?php

namespace Controller\ControlPanel\Game\Team;

use Lib\Core\Session;
use Lib\Data\Game;
use Lib\Data\Team;
use Lib\Exception\UnauthorizedException;

/**
 * Class Edit
 * @package Controller\ControlPanel\Game\Team
 */
final class Edit extends \Controller\ControlPanel
{
    /**
     * @return array
     * @throws UnauthorizedException
     */
    public function getArray(): array
    {
        $this->ensureLoggedIn();

        $gameId = $this->getVariable('gameId');
        $teamId = $this->getVariable('teamId');

        $game = $this->getGameRepo()->getByIdAndUserId($gameId, Session::getInstance()->getKey());
        if (!($game instanceof Game)) {
            throw new UnauthorizedException();
        }

        $team = new Team(0, '', '', 0, 0, 0, $game->getId());

        if ($teamId != 0) {
            $team = $this->getTeamRepo()->getByIdGameIdAndUserId($teamId, $game->getId(), Session::getInstance()->getKey());

            if (!($team instanceof Team)) {
                throw new UnauthorizedException();
            }
        }

        return array_merge(parent::getArray(), [
            'game' => $game,
            'team' => $team,
        ]);
    }
}
