<?php

namespace Controller\ControlPanel\Game;

use Lib\Core\Session;
use Lib\Data\Game;
use Lib\Exception\UnauthorizedException;

/**
 * Class Teams
 * @package Controller\ControlPanel\Game
 */
final class Teams extends \Controller\ControlPanel
{
    /**
     * @return array
     * @throws UnauthorizedException
     */
    public function getArray(): array
    {
        $this->ensureLoggedIn();

        $game = $this->getGameRepo()->getByIdAndUserId($this->getVariable('gameId'), Session::getInstance()->getKey());
        if (!($game instanceof Game)) {
            throw new UnauthorizedException();
        }

        return array_merge(parent::getArray(), [
            'game' => $game,
            'teams' => $this->getTeamRepo()->findByGameId($game->getId()),
        ]);
    }
}
