<?php

namespace Controller\ControlPanel;

use Lib\Core\Session;
use Lib\Exception\UnauthorizedException;

/**
 * Class ControlPanel
 * @package Controller
 */
class Game extends \Controller\ControlPanel
{
    /**
     * @return array
     * @throws UnauthorizedException
     */
    public function getArray(): array
    {
        $this->ensureLoggedIn();
        $userId = Session::getInstance()->getKey();

        $game = $this->getGameRepo()->getByIdAndUserId($this->getVariable('id'), $userId);
        if (!($game instanceof \Lib\Data\Game)) {
            throw new UnauthorizedException();
        }

        $teams = $this->getTeamRepo()->findByGameId($game->getId());
        $questions = $this->getQuestionRepo()->findByGameId($game->getId());

        return [
            'games' => $this->getGameRepo()->findByUserId($userId),
            'game' => $game,
            'teams' => $teams,
            'questions' => $questions,
        ];
    }
}
