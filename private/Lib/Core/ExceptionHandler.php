<?php

namespace Lib\Core;

/**
 * Class ExceptionHandler
 * @package Lib\Core
 */
final class ExceptionHandler extends \Lib\Core\Singleton
{
    /**
     * @param \Throwable $ex
     */
    public function handle(\Throwable $ex): void
    {
        print_r($ex);
    }
}
