<?php

namespace Lib\Core;

/**
 * Class Mailer
 * @package Lib\Core
 */
class Mailer
{
    /**
     * @param string $address
     * @param string $subject
     * @param string $message
     *
     * @return bool
     */
    public function mailToSubject(string $address, string $subject, string $message): bool
    {
        return mail($address, $subject, $message);
    }

    /**
     * @param array $addresses
     * @param string $subject
     * @param string $message
     *
     * @return bool
     */
    public function mailToMultipleAddresses(array $addresses, string $subject, string $message): bool
    {
        $isSuccess = true;
        foreach ($addresses as $address) {
            $isSuccess &= $this->mailToSubject($address, $subject, $message);
        }

        return $isSuccess;
    }
}
