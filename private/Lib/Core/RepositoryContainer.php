<?php

namespace Lib\Core;

use Lib\Repository;

/**
 * Class RepositoryContainer
 * @package Lib\Core
 */
abstract class RepositoryContainer
{
    /**
     * @var Repository\Answer
     */
    private $answerRepo;

    /**
     * @var Repository\Message
     */
    private $messageRepo;

    /**
     * @var Repository\BuyableObject
     */
    private $objectRepo;

    /**
     * @var Repository\Question
     */
    private $questionRepo;

    /**
     * @var Repository\Team
     */
    private $teamRepo;

    /**
     * @var Repository\User
     */
    private $userRepo;

    /**
     * @var Repository\Game
     */
    private $gameRepo;

    /**
     * @param Repository\Answer $repo
     */
    public function setAnswerRepo(Repository\Answer $repo): void
    {
        $this->answerRepo = $repo;
    }

    /**
     * @return Repository\Answer
     */
    public function getAnswerRepo(): Repository\Answer
    {
        if (!($this->answerRepo instanceof Repository\Answer)) {
            $this->setAnswerRepo(new Repository\Answer());
        }

        return $this->answerRepo;
    }

    /**
     * @param Repository\Message $repo
     */
    public function setMessageRepo(Repository\Message $repo): void
    {
        $this->messageRepo = $repo;
    }

    /**
     * @return Repository\Message
     */
    public function getMessageRepo(): Repository\Message
    {
        if (!($this->messageRepo instanceof Repository\Message)) {
            $this->setMessageRepo(new Repository\Message());
        }

        return $this->messageRepo;
    }

    /**
     * @param Repository\BuyableObject $repo
     */
    public function setObjectRepo(Repository\BuyableObject $repo): void
    {
        $this->objectRepo = $repo;
    }

    /**
     * @return \Lib\Repository\BuyableObject
     */
    public function getObjectRepo(): Repository\BuyableObject
    {
        if (!($this->objectRepo instanceof Repository\BuyableObject)) {
            $this->setObjectRepo(new Repository\BuyableObject());
        }

        return $this->objectRepo;
    }

    /**
     * @param Repository\Question $repo
     */
    public function setQuestionRepo(Repository\Question $repo): void
    {
        $this->questionRepo = $repo;
    }

    /**
     * @return Repository\Question
     */
    public function getQuestionRepo(): Repository\Question
    {
        if (!($this->questionRepo instanceof Repository\Question)) {
            $this->setQuestionRepo(new Repository\Question());
        }

        return $this->questionRepo;
    }

    /**
     * @param Repository\Team $repo
     */
    public function setTeamRepo(Repository\Team $repo): void
    {
        $this->teamRepo = $repo;
    }

    /**
     * @return Repository\Team
     */
    public function getTeamRepo(): Repository\Team
    {
        if (!($this->teamRepo instanceof Repository\Team)) {
            $this->setTeamRepo(new Repository\Team());
        }

        return $this->teamRepo;
    }

    /**
     * @param Repository\User $repo
     */
    public function setUserRepo(Repository\User $repo): void
    {
        $this->userRepo = $repo;
    }

    /**
     * @return Repository\User
     */
    public function getUserRepo(): Repository\User
    {
        if (!($this->userRepo instanceof Repository\User)) {
            $this->setUserRepo(new Repository\User());
        }

        return $this->userRepo;
    }

    /**
     * @param Repository\Game $repo
     */
    public function setGameRepo(Repository\Game $repo): void
    {
        $this->gameRepo = $repo;
    }

    /**
     * @return Repository\Game
     */
    public function getGameRepo(): Repository\Game
    {
        if (!($this->gameRepo instanceof Repository\Game)) {
            $this->setGameRepo(new Repository\Game());
        }

        return $this->gameRepo;
    }
}
