<?php

namespace Lib\Core;

/**
 * Class Validate
 * @package Lib\Core
 */
final class Validate extends Singleton
{
    /**
     * Checks whether or not the given variable is empty
     *
     * @param string $variable
     * @return bool
     */
    public function notEmpty($variable): bool
    {
        return !empty($variable);
    }

    /**
     * @param mixed $variable
     * @return bool
     */
    public function isEmpty($variable): bool
    {
        return empty($variable);
    }

    /**\
     * @param string $variable
     * @param int    $minLength
     * @return bool
     */
    public function isMinLength($variable, $minLength): bool
    {
        return strlen($variable) >= $minLength;
    }

    /**
     * @param string $variable
     * @param int    $maxLength
     * @return bool
     */
    public function isMaxLength($variable, $maxLength): bool
    {
        return strlen($variable) <= $maxLength;
    }

    /**
     * @param string $variable
     * @return bool
     */
    public function isValidPassword(string $variable): bool
    {
        $matches = preg_match('@[A-Z]@', $variable);
        $matches &= preg_match('@[a-z]@', $variable);
        $matches &= preg_match('@[0-9]@', $variable);
        $matches &= strlen($variable) > 8;

        return !!$matches;
    }

    /**
     * @param string $variable
     * @return bool
     */
    public function isValidEmailAddress(string $variable): bool
    {
        return !filter_var($variable, FILTER_VALIDATE_EMAIL) === false;
    }

    /**
     * @param int|string $date
     * @return bool
     */
    public function isValidDateInFuture($date): bool
    {
        if (is_string($date)) {
            return strtotime($date) > time();
        } elseif (is_int($date)) {
            return $date > time();
        }

        return false;
    }

    /**
     * @param int $value
     * @param int $minValue
     * @return bool
     */
    public function isMinValue(int $value, int $minValue): bool
    {
        return $value >= $minValue;
    }

    /**
     * @param int $value
     * @param int $maxValue
     * @return bool
     */
    public function isMaxValue(int $value, int $maxValue): bool
    {
        return $value <= $maxValue;
    }
}
