<?php

namespace Lib\Core\ExceptionHandler;

use Lib\Exception\UserException;

/**
 * Class Mailer
 * @package Lib\Core\ExceptionHandler
 */
final class Mailer implements ExceptionHandler
{
    const NAME = 'mail';

    /**
     * @var string
     */
    private $address;

    /**
     * @var \Lib\Core\Mailer
     */
    private $mailer;

    /**
     * @return \Lib\Core\Mailer
     */
    public function getMailer(): \Lib\Core\Mailer
    {
        if (!($this->mailer instanceof \Lib\Core\Mailer)) {
            $this->setMailer(new \Lib\Core\Mailer());
        }

        return $this->mailer;
    }

    public function setMailer(\Lib\Core\Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Mailer constructor.
     * @param string $address
     */
    public function __construct(string $address)
    {
        $this->address = $address;
    }

    /**
     * @param \Throwable $ex
     */
    public function handleException(\Throwable $ex): void
    {
        if ($ex instanceof UserException) {
            return;
        }

        $this->getMailer()->mailToSubject($this->address, "Exception: {$ex->getMessage()}", print_r($ex->getTrace()));
    }
}
