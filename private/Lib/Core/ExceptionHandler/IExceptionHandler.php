<?php

namespace Lib\Core\ExceptionHandler;

/**
 * Interface IExceptionHandler
 * @package Lib\Core\ExceptionHandler
 */
interface IExceptionHandler
{
    /**
     * @param \Throwable $ex
     */
    public function handleException(\Throwable $ex);
}
