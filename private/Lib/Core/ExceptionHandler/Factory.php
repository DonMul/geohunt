<?php

namespace Lib\Core\ExceptionHandler;

use Lib\Core\Settings;
use Lib\Core\Util;

/**
 * Class Factory
 * @package Lib\Core\ExceptionHandler
 */
final class Factory
{
    /**
     * @var ExceptionHandler
     */
    private static $instance;

    /**
     * @return ExceptionHandler
     */
    public static function getExceptionHandler(): ExceptionHandler
    {
        if (self::$instance instanceof ExceptionHandler) {
            return self::$instance;
        }

        $settings = Settings::getInstance()->get(['exceptions']);
        switch (Util::arrayGet($settings, 'handler')) {
            case Mailer::NAME:
            default:
                self::$instance = new Mailer(
                    Util::arrayGet($settings, 'address', '')
                );
                break;
        }

        return self::$instance;
    }
}
