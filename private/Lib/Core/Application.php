<?php

namespace Lib\Core;

/**
 * Class Application
 * @package Lib\Core
 */
final class Application extends \Lib\Core\Singleton
{
    /**
     * @var string
     */
    private $requestUrl;

    /**
     *
     */
    public function __construct()
    {
        $this->requestUrl = '/' . ltrim($_GET['_url'], '/');
        unset($_GET['_url']);
    }

    /**
     *
     */
    public function initiate(): void
    {
        Dispatcher::getInstance()->dispatchUrl($this->requestUrl);
    }
}
