<?php

namespace Lib\Core;

/**
 * Class Dispatcher
 * @package Lib\Core
 */
final class Dispatcher extends \Lib\Core\Singleton
{
    /**
     * @param string $url
     */
    public function dispatchUrl(string $url): void
    {
        $urlData = \Lib\Core\Sitemap::getInstance()->getDataForUrl($url);

        if (!$urlData) {
            $urlData = \Lib\Core\Sitemap::getInstance()->getDataForUrl('/404');
            http_response_code(404);
        }

        /**
         * @var \Lib\Core\BaseController $class
         */
        $class = new $urlData['controller'];
        $class->execute();
    }
}
