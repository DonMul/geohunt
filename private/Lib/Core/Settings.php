<?php

namespace Lib\Core;

use Symfony\Component\Yaml\Yaml;

/**
 * Class Settings
 * @package Lib\Core
 */
final class Settings extends \Lib\Core\Singleton
{
    /**
     * @var array
     */
    protected $settings = [];

    /**
     * @constructor
     */
    public function __construct()
    {
        $file = CONFROOT . 'settings.yaml';
        if (file_exists($file)) {
            $this->settings = Yaml::parse(file_get_contents($file));
        }
    }

    /**
     * Returns the value of the setting with the given name. This name can be a recursive pointer in the shape on an
     * associative array
     *
     * @param string|string[] $name
     * @param mixed           $default
     *
     * @return mixed
     */
    public function get($name, $default = null)
    {
        return \Lib\Core\Util::arrayGet($this->settings, $name, $default);
    }

    /**
     * @param array $settings
     */
    public function overrideSettings(array $settings)
    {
        $this->settings = array_merge($this->settings, $settings);
    }

    /**
     * @param string $key
     * @param mixed  $value
     */
    public function set(string $key, $value): void
    {
        $this->settings[$key] = $value;
    }

    /**
     * Return all settings
     *
     * @return array
     */
    public function getAll(): array
    {
        return $this->settings;
    }
}
