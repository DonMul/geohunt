<?php

namespace Lib\Core;

use Controller\FourOFour;
use Lib\Exception\PageNotFound;

/**
 * Class BaseController
 * @package Lib\Core
 */
abstract class BaseController extends RepositoryContainer
{
    /**
     * @var bool
     */
    private $requiresLogin = false;

    /**
     * @var array
     */
    protected $errors = [];

    /**
     * @param string|string[] $name
     * @param mixed $default
     *
     * @return mixed
     */
    protected function getVariable($name, $default = null)
    {
        return Util::arrayGet($_GET, $name, $default);
    }

    /**
     * @param boolean $requiresLogin
     */
    protected function setRequiresLogin(bool $requiresLogin): void
    {
        $this->requiresLogin = $requiresLogin;
    }

    /**
     * @return boolean
     */
    protected function getRequiresLogin(): bool
    {
        return $this->requiresLogin;
    }

    /**
     *
     */
    protected function validate()
    {
    }

    /**
     * Adds an validation error to the error list. If an empty $message is given, it will be ignored
     *
     * @param string $message
     * @param array  $params
     * @return bool
     */
    final protected function addError($message, $params = []):bool
    {
        if (empty($message)) {
            return false;
        }

        $this->errors[] = \Lib\Core\Translation::getInstance()->translate($message, $params);
        return true;
    }

    /**
     *
     */
    public function execute()
    {
        // Checks whether or not the user needs to be logged in to view this page. If the user requires to be logged in
        // and he/she is not, a Login page will be shown
        if ($this->getRequiresLogin() === true && \Lib\Core\Session::getInstance()->isLoggedIn() === false) {
            $this->serveTemplate('User/Login.html.twig', []);
            return;
        }

        // Validates and executes the page. If anything goes wrong, or the validation did not pass, an error page will
        // be shown
        try {
            $this->validate();

            if (!empty($this->errors)) {
                $this->showErrorPage($this->errors);
                return;
            }

            try {
                $data = $this->getArray();
            } catch (PageNotFound $exception) {
                header("HTTP/1.1 404 Not Found");
                $controller = new FourOFour();
                $controller->execute();
                exit;
            }
            $this->serveTemplate(str_replace('Controller\\', '', get_called_class()) . '.html.twig', $data);
        } catch (\Throwable $ex) {
            ExceptionHandler\Factory::getExceptionHandler()->handleException($ex);
            $this->showErrorPage([$ex->getMessage()]);
        }
    }

    /**
     * Shows an error page
     */
    protected function showErrorPage(array $errors): void
    {
        http_response_code(500);
        $this->serveTemplate('error.html.twig', ['errors' => $errors]);
    }

    /**
     * Serves the given template with the given context added to it. The context must consits out of data you want to
     * be available in the FrontEnd
     *
     * @param string $templateLocation
     * @param array  $context
     */
    protected function serveTemplate(string $templateLocation, array $context): void
    {
        $array = [
            'context' => $context,
            'languages' => \Lib\Core\Translation::getInstance()->getAllLanguages(),
            'request' => $_GET,
            'loggedIn' => \Lib\Core\Session::getInstance()->isLoggedIn(),
            'language' => \Lib\Core\Translation::getInstance()->getLanguage(),
        ];

        if ($this->getTitle() !== null) {
            $array['title'] = $this->getTitle();
        }

        if ($this->getDescription() !== null) {
            $array['description'] = $this->getDescription();
        }

        $array = $this->sanitizeContext($array);

        // If the context GET variable is set and the logged in user is an administrator, the context is printed and the
        // template is not executed.
        if (isset($_GET['context'])) {
            echo '<pre>';
            print_r($array);
            echo '</pre>';
            exit;
        }

        $this->render($templateLocation, $array);
    }

    /**
     * @param string $templateLocation
     * @param array $data
     */
    protected function render(string $templateLocation, array $data): void
    {
        try {
            $loader = new \Twig_Loader_Filesystem(TEMPLATEROOT);
            $twig = new \Twig_Environment($loader, [
                'cache' => false
            ]);

            // Add custom twig functions to this page
            $this->addTwigFunctions($twig);// Load Template
            $template = $twig->loadTemplate($templateLocation);

            // Render the template
            echo $template->render($data);
        } catch (\Throwable $exception) {
            ExceptionHandler\Factory::getExceptionHandler()->handleException($exception);
        }
    }


    /**
     * Add custom functions to the twig environment
     *
     * @param \Twig_Environment $twig
     */
    private function addTwigFunctions(\Twig_Environment $twig): void
    {
        $translate = new \Twig_SimpleFunction('t', [\Lib\Core\Translation::getInstance(), 'translate']);
        $settings = new \Twig_SimpleFunction('s', [\Lib\Core\Settings::getInstance(), 'get']);
        $translateUrl = new \Twig_SimpleFunction('tl', [\Lib\Core\Translation::getInstance(), 'translateLink']);

        $twig->addFunction($translate);
        $twig->addFunction($settings);
        $twig->addFunction($translateUrl);
        $twig->addFunction(new \Twig_SimpleFunction('md5', [$this, 'md5']));
    }

    /**
     * @param string $variable
     * @return string
     */
    public function md5(string $variable): string
    {
        return md5($variable);
    }

    /**
     * @return array
     */
    abstract public function getArray(): array;

    /**
     * Sanitizes the content recursively
     *
     * @param array $context
     * @return array
     */
    private function sanitizeContext(array $context): array
    {
        foreach ($context as &$data) {
            if (is_array($data)) {
                $data = $this->sanitizeContext($data);
            } elseif (is_object($data)) {
                if (method_exists($data, 'toArray')) {
                    $data = $data->toArray();
                }
            }
        }

        return $context;
    }

    /**
     * @return string
     */
    protected function getTitle(): string
    {
        return '';
    }

    /**
     * @return string
     */
    protected function getDescription(): string
    {
        return '';
    }
}
