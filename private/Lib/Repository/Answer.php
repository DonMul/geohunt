<?php

namespace Lib\Repository;

use Lib\Data;

/**
 * Class Answer
 * @package Lib\Repository
 */
final class Answer extends BaseRepository
{
    const TABLENAME = 'question_answers';

    /**
     * @return string
     */
    private function getTableName(): string
    {
        return $this->getDatabase()->getFullTableName(self::TABLENAME);
    }

    /**
     * @param array $data
     *
     * @return Data\Answer
     */
    private function bindSqlResult(array $data): Data\Answer
    {
        return new Data\Answer(
            $data['id'],
            $data['questionId'],
            $data['position'],
            $data['answer'],
            !!$data['isCorrect']
        );
    }

    /**
     * @param array $data
     *
     * @return Data\TeamAnswer
     */
    private function bindTeamQuestionSqlResult(array $data): Data\TeamAnswer
    {
        return new Data\TeamAnswer(
            $data['id'],
            $data['teamId'],
            $data['questionId'],
            $data['answer'],
            $data['answerDate'],
            !!$data['isHandled'],
            !!$data['isCorrect']
        );
    }

    /**
     * @param int $questionId
     * @return array
     */
    public function findByQuestionId(int $questionId): array
    {
        $data = $this->getDatabase()->fetchAll(
            "SELECT * FROM `" . $this->getTableName() . "` WHERE questionId = ? ORDER BY `position` ASC",
            [
                $questionId
            ],
            'i'
        );

        $questions = [];
        foreach ($data as $question) {
            $questions[] = $this->bindSqlResult($question);
        }

        return $questions;
    }

    /**
     * @param Data\Team $team
     * @param Data\Question $question
     * @param string $answer
     * @param bool $isCorrect
     * @param bool $isHandled
     *
     * @return bool
     */
    public function ensureQuestionAnswered(Data\Team $team, Data\Question $question, string $answer, bool $isCorrect, bool $isHandled = false): bool
    {
        $query = "INSERT INTO `" . $this->getDatabase()->getFullTableName('team_question') . "` (teamId, questionId, answer, isCorrect, isHandled) " .
            "SELECT * FROM (SELECT " .
                $team->getId() . " AS teamId, " .
                $question->getId() . " AS questionId, '" .
                $this->getDatabase()->escape($answer) . "' AS answer, " .
                intval($isCorrect) . " AS isCorrect, " .
                intval($isHandled) . " AS isHandled) AS tmp " .
            "WHERE NOT EXISTS (" .
            "SELECT id FROM " . $this->getDatabase()->getFullTableName('team_question') . " " .
            "WHERE teamId = ? and questionId = ?" .
            ") LIMIT 1";

        $result = $this->getDatabase()->query(
            $query,
            [
                $team->getId(),
                $question->getId()
            ],
            'ii'
        );

        return $result->affected_rows > 0;
    }

    /**
     * @return array
     */
    public function getAnsweredInfo(): array
    {
        $tq = $this->getDatabase()->getFullTableName('team_question');
        $t = $this->getDatabase()->getFullTableName('team');
        $q = $this->getDatabase()->getFullTableName('question');
        $query = "
            SELECT * FROM (
                SELECT {$t}.name AS teamName, {$t}.id AS tId, {$q}.title AS question, {$q}.id AS qId, {$q}.type AS type
                FROM {$q}
                LEFT JOIN {$t} ON 1 = 1
            ) AS tmp
            LEFT JOIN {$tq} ON {$tq}.teamId = tmp.tId AND {$tq}.questionId = tmp.qId 
            ORDER BY {$tq}.answerDate DESC
        ";

        return $this->getDatabase()->fetchAll($query);
    }

    /**
     * @return array
     */
    public function getAnsweredInfoForGame(Data\Game $game): array
    {
        $tq = $this->getDatabase()->getFullTableName('team_question');
        $t = $this->getDatabase()->getFullTableName('team');
        $q = $this->getDatabase()->getFullTableName('question');
        $query = "
            SELECT * FROM (
                SELECT {$t}.name AS teamName, {$t}.id AS tId, {$q}.title AS question, {$q}.id AS qId, {$q}.type AS type
                FROM {$q}
                LEFT JOIN {$t} ON 1 = 1
            ) AS tmp
            LEFT JOIN {$tq} ON {$tq}.teamId = tmp.tId AND {$tq}.questionId = tmp.qId
            WHERE {$q}.gameId = ?
            ORDER BY {$tq}.answerDate DESC
        ";

        return $this->getDatabase()->fetchAll(
            $query,
            [
                $game
            ],
            'i'
        );
    }

    /**
     * @param Data\Team $team
     * @param Data\Question $question
     */
    public function removeAnswerForTeam(Data\Team $team, Data\Question $question): void
    {
        $this->getDatabase()->query(
            "DELETE FROM `" . $this->getDatabase()->getFullTableName('team_question') . "` WHERE  teamId = ? and questionId = ?",
            [
                $team->getId(),
                $question->getId(),
            ],
            'ii'
        );
    }

    /**
     * @param Data\Team $team
     * @param Data\Question $question
     *
     * @return Data\TeamAnswer|null
     */
    public function getAnserToQuestionByTeam(Data\Team $team, Data\Question $question): ?Data\TeamAnswer
    {
        $data = $this->getDatabase()->fetchOne(
            "SELECT * FROM `" . $this->getDatabase()->getFullTableName('team_question') . "` WHERE teamId = ? and questionId = ?",
            [
                $team->getId(),
                $question->getId(),
            ],
            'ii'
        );

        if (!$data) {
            return null;
        }

        return $this->bindTeamQuestionSqlResult($data);
    }

    /**
     * @param int $id
     * @return Data\Answer
     */
    public function getById(int $id): ?Data\Answer
    {
        $data = $this->getDatabase()->fetchOne(
            "SELECT * FROM `" . $this->getTableName() . "` WHERE id = ?",
            [$id],
            'i'
        );

        if (!$data) {
            return null;
        }

        return $this->bindSqlResult($data);
    }

    /**
     * @param int $id
     * @param int $questionId
     *
     * @return Data\Answer
     */
    public function getByIdAndQuestionId(int $id, int $questionId): ?Data\Answer
    {
        $data = $this->getDatabase()->fetchOne(
            "SELECT * FROM `" . $this->getTableName() . "` WHERE id = ? AND questionId = ?",
            [$id, $questionId],
            'ii'
        );

        if (!$data) {
            return null;
        }

        return $this->bindSqlResult($data);
    }

    /**
     * @param Data\Answer $answer
     */
    public function save(Data\Answer $answer): void
    {
        $db = $this->getDatabase();
        $params = [
            $answer->getQuestionId(),
            $answer->getPosition(),
            $answer->getAnswer(),
            intval($answer->isCorrect()),
        ];

        $types = 'iisi';
        if ($answer->getId() === null || $answer->getId() === 0) {
            $sql = "INSERT INTO `" . $this->getTableName() . "` (`questionId`, `position`, `answer`, `isCorrect` ) VALUES ( ?, ?, ?, ? )";
            $result = $db->query($sql, $params, $types);
            $answer->setId($result->insert_id);
        } else {
            $sql = "UPDATE `" . $this->getTableName() . "` SET `questionId` = ?, `position` = ?, `answer` = ?, `isCorrect` = ? WHERE `id` = ?";
            $params[] = $answer->getId();
            $types .= 'i';
            $db->query($sql, $params, $types);
        }
    }

    /**
     *
     */
    public function delete(Data\Answer $answer): bool
    {
        $result = $this->getDatabase()->query(
            "DELETE FROM `" . $this->getTableName() . "` WHERE id = ?",
            [$answer->getId()],
            'i'
        );

        return $result->affected_rows > 0;
    }
}
