<?php

namespace Lib\Repository;

use Lib\Data;

/**
 * Class Question
 * @package Lib\Repository
 */
final class Question extends BaseRepository
{
    const TABLENAME = 'question';

    /**
     * @return string
     */
    private function getTableName(): string
    {
        return $this->getDatabase()->getFullTableName(self::TABLENAME);
    }

    /**
     * @param array $data
     * @return Data\Question
     */
    private function bindSqlResult(array $data): Data\Question
    {
        return new Data\Question(
            $data['id'],
            $data['title'],
            $data['description'],
            $data['value'],
            $data['lat'],
            $data['lon'],
            $data['type'],
            $data['gameId']
        );
    }

    /**
     * @return Data\Question[]
     */
    public function getAll(): array
    {
        $data = $this->getDatabase()->fetchAll(
            "SELECT * FROM `" . $this->getTableName() . "`"
        );

        $teams = [];
        foreach ($data as $team) {
            $teams[] = $this->bindSqlResult($team);
        }

        return $teams;
    }

    /**
     * @param int $id
     * @return Data\Question|null
     */
    public function getById(int $id): ?Data\Question
    {
        $data = $this->getDatabase()->fetchOne(
            "SELECT * FROM `" . $this->getTableName() . "` WHERE id = ?",
            [$id],
            'i'
        );

        if (!$data) {
            return null;
        }

        return $this->bindSqlResult($data);
    }

    /**
     * @param int $id
     * @return Data\Game
     */
    public function getAmountForUser(int $userId): int
    {
        $data = $this->getDatabase()->fetchOne(
            "SELECT count(1) AS amount FROM `" . $this->getTableName() .
            "` LEFT JOIN " . $this->getDatabase()->getFullTableName('game') .
            " ON " . $this->getDatabase()->getFullTableName('game') . ".id = " . $this->getTableName() . ".gameId " .
            "WHERE userId = ?",
            [$userId],
            'i'
        );

        return intval($data['amount']);
    }

    /**
     * @return Data\Team[]
     */
    public function findByGameId(int $gameId): array
    {
        $data = $this->getDatabase()->fetchAll(
            "SELECT * FROM `" . $this->getTableName() . "` WHERE gameId = ?",
            [$gameId],
            'i'
        );

        $teams = [];
        foreach ($data as $team) {
            $teams[] = $this->bindSqlResult($team);
        }

        return $teams;
    }
    
    /**
     * @param Data\Question $question
     */
    public function save(Data\Question $question): void
    {
        $db = $this->getDatabase();
        $params = [
            $question->getTitle(),
            $question->getType(),
            $question->getLon(),
            $question->getLat(),
            $question->getValue(),
            $question->getDescription(),
            $question->getGameId(),
        ];

        $types = 'ssssisi';
        if ($question->getId() === null || $question->getId() === 0) {
            $sql = "INSERT INTO `" . $this->getTableName() . "` (`title`, `type`, `lon`, `lat`, `value`, `description`, `gameId`) VALUES ( ?, ?, ?, ?, ?, ?, ? )";
            $result = $db->query($sql, $params, $types);
            $question->setId($result->insert_id);
        } else {
            $sql = "UPDATE `" . $this->getTableName() . "` SET `title` = ?, `type` = ?, `lon` = ?, `lat` = ?, `value` = ?, `description` = ?, `gameId` = ? WHERE `id` = ?";
            $params[] = $question->getId();
            $types .= 'i';
            $db->query($sql, $params, $types);
        }
    }

    /**
     * @param int $teamId
     * @param int $gameId
     * @param int $userId
     *
     * @return Data\Question
     */
    public function getByIdGameIdAndUserId(int $teamId, int $gameId, int $userId): ?Data\Question
    {
        $data = $this->getDatabase()->fetchOne(
            "SELECT `" . $this->getTableName() . "`.* FROM `" . $this->getTableName() .
            "` LEFT JOIN `" . $this->getDatabase()->getFullTableName('game') . "` " .
            "ON `" . $this->getDatabase()->getFullTableName('game') . "`.id = `". $this->getTableName() . "`.gameId " .
            "WHERE `" . $this->getTableName() . "`.id = ? " .
            "AND `" . $this->getDatabase()->getFullTableName('game') . "`.id = ? " .
            "AND `" . $this->getDatabase()->getFullTableName('game') . "`.userId = ?",
            [
                $teamId,
                $gameId,
                $userId
            ],
            'iii'
        );

        if (!$data) {
            return null;
        }

        return $this->bindSqlResult($data);
    }
}
