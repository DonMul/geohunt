<?php

namespace Lib\Repository;

use Lib\Data;

/**
 * Class Message
 * @package Lib\Repository
 */
final class BuyableObject extends BaseRepository
{
    const TABLENAME = 'object';

    /**
     * @return string
     */
    private function getTableName(): string
    {
        return $this->getDatabase()->getFullTableName(self::TABLENAME);
    }

    /**
     * @param int $id
     * @return Data\BuyableObject
     */
    public function getById(int $id): ?Data\BuyableObject
    {
        $data = $this->getDatabase()->fetchOne(
            "SELECT * FROM `" . $this->getTableName() . "` WHERE id = ?",
            [$id],
            'i'
        );

        if (!$data) {
            return null;
        }

        return $this->bindSqlResult($data);
    }

    /**
     * @param array $data
     * @return Data\BuyableObject
     */
    private function bindSqlResult(array $data): Data\BuyableObject
    {
        return new Data\BuyableObject(
            $data['id'],
            $data['name'],
            $data['image'],
            $data['purchaseValue'],
            $data['points'],
            $data['gameId']
        );
    }

    /**
     * @param Data\Message $message
     *
     * @return bool
     */
    public function delete(Data\Message $message): bool
    {
        $result = $this->getDatabase()->query(
            "DELETE FROM `" . $this->getTableName() . "` WHERE id = ?",
            [$message->getId()],
            'i'
        );

        return $result->affected_rows > 0;
    }

    /**
     * @param Data\BuyableObject $team
     */
    public function save(Data\BuyableObject $object)
    {
        $db = $this->getDatabase();
        $params = [
            $object->getName(),
            $object->getImage(),
            $object->getPurchaseValue(),
            $object->getPoints(),
            $object->getGameId(),
        ];

        $types = 'ssiii';
        if ($object->getId() === null || $object->getId() === 0) {
            $sql = "INSERT INTO `" . $this->getTableName() . "` (`name`, `image`, `purchaseValue`, `points`, `gameId` ) VALUES ( ?, ?, ?, ?, ? )";
            $result = $db->query($sql, $params, $types);
            $object->setId($result->insert_id);
        } else {
            $sql = "UPDATE `" . $this->getTableName() . "` SET `name` = ?, `image` = ?, `purchaseValue` = ?, `points` = ?, `gameId` = ? WHERE `id` = ?";
            $params[] = $object->getId();
            $types .= 'i';
            $db->query($sql, $params, $types);
        }
    }


    /**
     * @param Data\Team $team
     * @return array
     */
    public function getAllByTeam(Data\Team $team): array
    {
        $query = "SELECT *, " . $this->getTableName() . ".id AS id FROM `" . $this->getTableName() .
            "` LEFT JOIN " . $this->getDatabase()->getFullTableName('team_object') .
            " ON `" . $this->getDatabase()->getFullTableName('team_object') . "`.objectId = `" . $this->getTableName() . "`.id " .
            "AND `" . $this->getDatabase()->getFullTableName('team_object') . "`.teamId = ? " .
            "WHERE `" . $this->getTableName() . "`.gameId = ?";

        $data = $this->getDatabase()->fetchAll(
            $query,
            [
                $team->getId(),
                $team->getGameId(),
            ],
            'ii'
        );

        return $data;
    }

    /**
     * @param Data\Team $team
     * @param Data\BuyableObject $object
     *
     * @return array
     */
    public function getObjectForTeam(Data\Team $team, Data\BuyableObject $object): array
    {
        $data = $this->getDatabase()->fetchOne(
            "SELECT * FROM `" . $this->getDatabase()->getFullTableName('team_object') . "` WHERE teamId = ? AND objectId = ?",
            [
                $team->getId(),
                $object->getId()
            ],
            'ii'
        );

        if (!$data) {
            return null;
        }

        return $data;
    }

    /**
     * @param Data\Team $team
     * @param Data\BuyableObject $object
     */
    public function addObjectToTeam(Data\Team $team, Data\BuyableObject $object)
    {
        $db = $this->getDatabase();
        $db->query(
            "INSERT INTO `" . $this->getDatabase()->getFullTableName('team_object') . "` (`teamId`, `objectId`, `amount`) VALUES ( ?, ?, ? )",
            [
                $team->getId(),
                $object->getId(),
                1
            ],
            'iii'
        );
    }

    /**
     * @param Data\Team $team
     * @param Data\BuyableObject $object
     * @param int $amount
     */
    public function increaseTeamObjectAmount(Data\Team $team, Data\BuyableObject $object, $amount = 1): int
    {
        $db = $this->getDatabase();
        $db->query(
            "UPDATE `" . $this->getDatabase()->getFullTableName('team_object') . "` SET amount = amount + " . $amount . " WHERE teamId = ? AND objectId = ? ",
            [
                $team->getId(),
                $object->getId(),
            ],
            'ii'
        );
    }

    /**
     * @param int $userId
     * @return int
     */
    public function getAmountForUser(int $userId): int
    {
        $data = $this->getDatabase()->fetchOne(
            "SELECT count(1) AS amount FROM `" . $this->getTableName() .
            "` LEFT JOIN " . $this->getDatabase()->getFullTableName('game') .
            " ON " . $this->getDatabase()->getFullTableName('game') . ".id = " . $this->getTableName() . ".gameId " .
            "WHERE userId = ?",
            [$userId],
            'i'
        );

        return intval($data['amount']);
    }

    /**
     * @return Data\BuyableObject[]
     */
    public function findByGameId(int $gameId): array
    {
        $data = $this->getDatabase()->fetchAll(
            "SELECT * FROM `" . $this->getTableName() . "` WHERE gameId = ?",
            [$gameId],
            'i'
        );

        $teams = [];
        foreach ($data as $team) {
            $teams[] = $this->bindSqlResult($team);
        }

        return $teams;
    }

    /**
     * @param int $teamId
     * @param int $gameId
     * @param int $userId
     *
     * @return Data\BuyableObject
     */
    public function getByIdGameIdAndUserId(int $teamId, int $gameId, int $userId): ?Data\BuyableObject
    {
        $data = $this->getDatabase()->fetchOne(
            "SELECT `" . $this->getTableName() . "`.* FROM `" . $this->getTableName() .
            "` LEFT JOIN `" . $this->getDatabase()->getFullTableName('game') . "` " .
            "ON `" . $this->getDatabase()->getFullTableName('game') . "`.id = `". $this->getTableName() . "`.gameId " .
            "WHERE `" . $this->getTableName() . "`.id = ? " .
            "AND `" . $this->getDatabase()->getFullTableName('game') . "`.id = ? " .
            "AND `" . $this->getDatabase()->getFullTableName('game') . "`.userId = ?",
            [
                $teamId,
                $gameId,
                $userId
            ],
            'iii'
        );

        if (!$data) {
            return null;
        }

        return $this->bindSqlResult($data);
    }
}
