<?php

namespace Lib\Repository;

use Lib\Data;

/**
 * Class Team
 * @package Lib\Repository
 */
final class Game extends BaseRepository
{
    const TABLENAME = 'game';

    /**
     * @return string
     */
    private function getTableName(): string
    {
        return $this->getDatabase()->getFullTableName(self::TABLENAME);
    }

    /**
     * @param int $id
     *
     * @return Data\Game
     */
    public function getById(int $id): ?Data\Game
    {
        $data = $this->getDatabase()->fetchOne(
            "SELECT * FROM `" . $this->getTableName() . "` WHERE id = ?",
            [$id],
            'i'
        );

        if (!$data) {
            return null;
        }

        return $this->bindSqlResult($data);
    }

    /**
     * @param int $id
     * @param int $userId
     *
     * @return Data\Game
     */
    public function getByIdAndUserId(int $id, int $userId): ?Data\Game
    {
        $data = $this->getDatabase()->fetchOne(
            "SELECT * FROM `" . $this->getTableName() . "` WHERE id = ? AND userId = ?",
            [
                $id,
                $userId,
            ],
            'ii'
        );

        if (!$data) {
            return null;
        }

        return $this->bindSqlResult($data);
    }

    /**
     * @param array $data
     *
     * @return Data\Game
     */
    private function bindSqlResult(array $data): Data\Game
    {
        return new Data\Game(
            $data['id'],
            $data['userId'],
            $data['name'],
            $data['startTime'],
            $data['endTime']
        );
    }

    /**
     * @param Data\Game $game
     *
     * @return bool
     */
    public function delete(Data\Game $game): bool
    {
        $result = $this->getDatabase()->query(
            "DELETE FROM `" . $this->getTableName() . "` WHERE id = ?",
            [$game->getId()],
            'i'
        );

        return $result->affected_rows > 0;
    }

    /**
     * @param Data\Team $team
     */
    public function save(Data\Game $game): void
    {
        $db = $this->getDatabase();
        $params = [
            $game->getUserId(),
            $game->getName(),
            $game->getStartTime(),
            $game->getEndTime(),
        ];

        $types = 'isss';
        if ($game->getId() === null || $game->getId() === 0) {
            $sql = "INSERT INTO `" . $this->getTableName() . "` (`userId`, `name`, `startTime`, `endTime`) VALUES ( ?, ?, ?, ? )";
            $result = $db->query($sql, $params, $types);
            $game->setId($result->insert_id);
        } else {
            $sql = "UPDATE `" . $this->getTableName() . "` SET `userId` = ?, `name` = ?, `startTime` = ?, `endTime` = ? WHERE `id` = ?";
            $params[] = $game->getId();
            $types .= 'i';
            $db->query($sql, $params, $types);
        }
    }

    /**
     * @param int $userId
     *
     * @return Data\Game[]
     */
    public function findByUserId(int $userId): array
    {
        $data = $this->getDatabase()->fetchAll(
            "SELECT * FROM `" . $this->getTableName() . "` WHERE userId = ?",
            [$userId],
            'i'
        );

        $games = [];
        foreach ($data as $game) {
            $games[] = $this->bindSqlResult($game);
        }

        return $games;
    }

    /**
     * @param int $userId
     *
     * @return int
     */
    public function getAmountForUser(int $userId): int
    {
        $data = $this->getDatabase()->fetchOne(
            "SELECT count(1) AS amount FROM `" . $this->getTableName() . "` WHERE userId = ?",
            [$userId],
            'i'
        );

        return intval($data['amount']);
    }

    /**
     * @return Data\Game[]
     */
    public function getAll(): array
    {
        $data = $this->getDatabase()->fetchAll(
            "SELECT * FROM `" . $this->getTableName() . "`"
        );

        $teams = [];
        foreach ($data as $team) {
            $teams[] = $this->bindSqlResult($team);
        }

        return $teams;
    }
}
