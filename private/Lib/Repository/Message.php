<?php

namespace Lib\Repository;

use Lib\Data;

/**
 * Class Message
 * @package Lib\Repository
 */
final class Message extends BaseRepository
{
    const TABLENAME = 'message';

    /**
     * @return string
     */
    private function getTableName(): string
    {
        return $this->getDatabase()->getFullTableName(self::TABLENAME);
    }

    /**
     * @param int $id
     *
     * @return Data\Message
     */
    public function getById(int $id): ?Data\Message
    {
        $data = $this->getDatabase()->fetchOne(
            "SELECT * FROM `" . $this->getTableName() . "` WHERE id = ?",
            [$id],
            'i'
        );

        if (!$data) {
            return null;
        }

        return $this->bindSqlResult($data);
    }

    /**
     * @param array $data
     *
     * @return Data\Message
     */
    private function bindSqlResult(array $data): Data\Message
    {
        return new Data\Message(
            $data['id'],
            $data['teamId'],
            $data['message'],
            $data['creationDate']
        );
    }

    /**
     * @param Data\Message $message
     * @return bool
     */
    public function delete(Data\Message $message): bool
    {
        $result = $this->getDatabase()->query(
            "DELETE FROM `" . $this->getTableName() . "` WHERE id = ?",
            [$message->getId()],
            'i'
        );

        return $result->affected_rows > 0;
    }

    /**
     * @param Data\Message $message
     */
    public function save(Data\Message $message): void
    {
        $db = $this->getDatabase();
        $params = [
            $message->getTeamId(),
            $message->getMessage(),
        ];

        $types = 'ssi';
        if ($message->getId() === null || $message->getId() === 0) {
            $sql = "INSERT INTO `" . $this->getTableName() . "` (`teamId`, `message` ) VALUES ( ?, ? )";
            $result = $db->query($sql, $params, $types);
            $message->setId($result->insert_id);
        } else {
            $sql = "UPDATE `" . $this->getTableName() . "` SET `teamId` = ?, `message` = ? WHERE `id` = ?";
            $params[] = $message->getId();
            $types .= 'i';
            $db->query($sql, $params, $types);
        }
    }


    /**
     * @param Data\Team $team
     * @param string $dateTime
     *
     * @return array
     */
    public function getAllForTeamAfterDate(Data\Team $team, string $dateTime): array
    {
        $data = $this->getDatabase()->fetchAll(
            "SELECT * FROM `" . $this->getTableName() . "` WHERE teamId = ? AND creationDate > ?",
            [
                $team->getId(),
                $dateTime
            ],
            'is'
        );

        $teams = [];
        foreach ($data as $team) {
            $teams[] = $this->bindSqlResult($team);
        }

        return $teams;
    }
}
