<?php

namespace Lib\Repository;

use Lib\Data;

/**
 * Class Team
 * @package Lib\Repository
 */
final class Team extends BaseRepository
{
    const TABLENAME = 'team';

    /**
     * @return string
     */
    private function getTableName(): string
    {
        return $this->getDatabase()->getFullTableName(self::TABLENAME);
    }

    /**
     * @param int $id
     * @return Data\Team|null
     */
    public function getById(int $id): ?Data\Team
    {
        $data = $this->getDatabase()->fetchOne(
            "SELECT * FROM `" . $this->getTableName() . "` WHERE id = ?",
            [$id],
            'i'
        );

        if (!$data) {
            return null;
        }

        return $this->bindSqlResult($data);
    }

    /**
     * @param array $data
     * @return Data\Team
     */
    private function bindSqlResult(array $data): Data\Team
    {
        return new Data\Team(
            $data['id'],
            $data['name'],
            $data['code'],
            $data['points'],
            $data['lat'],
            $data['lon'],
            $data['gameId']
        );
    }

    /**
     * @param Data\Team $team
     * @return bool
     */
    public function delete(Data\Team $team): bool
    {
        $result = $this->getDatabase()->query(
            "DELETE FROM `" . $this->getTableName() . "` WHERE id = ?",
            [$team->getId()],
            'i'
        );

        return $result->affected_rows > 0;
    }

    /**
     * @param string $code
     * @return Data\Team
     */
    public function getByCode(string $code): ?Data\Team
    {
        $data = $this->getDatabase()->fetchOne(
            "SELECT * FROM `" . $this->getTableName() . "` WHERE code = ?",
            [$code],
            's'
        );

        if (!$data) {
            return null;
        }

        return $this->bindSqlResult($data);
    }

    /**
     * @param Data\Team $team
     */
    public function save(Data\Team $team): void
    {
        $db = $this->getDatabase();
        $params = [
            $team->getName(),
            $team->getCode(),
            $team->getPoints(),
            $team->getLat(),
            $team->getLon(),
            $team->getGameId()
        ];

        $types = 'ssissi';
        if ($team->getId() === null || $team->getId() === 0) {
            $sql = "INSERT INTO `" . $this->getTableName() . "` (`name`, `code`, `points`, `lat`, `lon`, `gameId`) VALUES ( ?, ?, ?, ?, ?, ? )";
            $result = $db->query($sql, $params, $types);
            $team->setId($result->insert_id);
        } else {
            $sql = "UPDATE `" . $this->getTableName() . "` SET `name` = ?, `code` = ?, `points` = ?, `lat` = ?, `lon` = ?, `gameId` = ? WHERE `id` = ?";
            $params[] = $team->getId();
            $types .= 'i';
            $db->query($sql, $params, $types);
        }
    }

    /**
     * @param int $id
     * @return Data\Game
     */
    public function getAmountForUser(int $userId): int
    {
        $data = $this->getDatabase()->fetchOne(
            "SELECT count(1) AS amount FROM `" . $this->getTableName() .
            "` LEFT JOIN " . $this->getDatabase()->getFullTableName('game') .
            " ON " . $this->getDatabase()->getFullTableName('game') . ".id = " . $this->getTableName() . ".gameId " .
            "WHERE userId = ?",
            [$userId],
            'i'
        );

        return intval($data['amount']);
    }

    /**
     * @return Data\Team[]
     */
    public function findByGameId(int $gameId): array
    {
        $data = $this->getDatabase()->fetchAll(
            "SELECT * FROM `" . $this->getTableName() . "` WHERE gameId = ?",
            [$gameId],
            'i'
        );

        $teams = [];
        foreach ($data as $team) {
            $teams[] = $this->bindSqlResult($team);
        }

        return $teams;
    }

    /**
     * @return Data\Team[]
     */
    public function getAll(): array
    {
        $data = $this->getDatabase()->fetchAll(
            "SELECT * FROM `" . $this->getTableName() . "`"
        );

        $teams = [];
        foreach ($data as $team) {
            $teams[] = $this->bindSqlResult($team);
        }

        return $teams;
    }

    /**
     * @param int $teamId
     * @param int $gameId
     * @param int $userId
     * @return Data\Team
     */
    public function getByIdGameIdAndUserId(int $teamId, int $gameId, int $userId): Data\Team
    {
        $data = $this->getDatabase()->fetchOne(
            "SELECT `" . $this->getTableName() . "`.* FROM `" . $this->getTableName() .
            "` LEFT JOIN `" . $this->getDatabase()->getFullTableName('game') . "` " .
            "ON `" . $this->getDatabase()->getFullTableName('game') . "`.id = `". $this->getTableName() . "`.gameId " .
            "WHERE `" . $this->getTableName() . "`.id = ? " .
            "AND `" . $this->getDatabase()->getFullTableName('game') . "`.id = ? " .
            "AND `" . $this->getDatabase()->getFullTableName('game') . "`.userId = ?",
            [
                $teamId,
                $gameId,
                $userId
            ],
            'iii'
        );

        if (!$data) {
            return null;
        }

        return $this->bindSqlResult($data);
    }
}
