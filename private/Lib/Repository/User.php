<?php

namespace Lib\Repository;

use Lib\Data;

/**
 * Class Team
 * @package Lib\Repository
 */
final class User extends BaseRepository
{
    const TABLENAME = 'user';

    /**
     * @return string
     */
    private function getTableName(): string
    {
        return $this->getDatabase()->getFullTableName(self::TABLENAME);
    }

    /**
     * @param int $id
     * @return Data\User
     */
    public function getById(int $id): ?Data\User
    {
        $data = $this->getDatabase()->fetchOne(
            "SELECT * FROM `" . $this->getTableName() . "` WHERE id = ?",
            [$id],
            'i'
        );

        if (!$data) {
            return null;
        }

        return $this->bindSqlResult($data);
    }

    /**
     * @param string $username
     * @return Data\User|null
     */
    public function getByUsername(string $username): ?Data\User
    {
        $data = $this->getDatabase()->fetchOne(
            "SELECT * FROM `" . $this->getTableName() . "` WHERE username = ?",
            [$username],
            's'
        );

        if (!$data) {
            return null;
        }

        return $this->bindSqlResult($data);
    }

    /**
     * @param array $data
     * @return Data\User
     */
    private function bindSqlResult(array $data): Data\User
    {
        return new Data\User(
            $data['id'],
            $data['username'],
            $data['password'],
            $data['emailAddress'],
            $data['role']
        );
    }

    /**
     * @param Data\Team $team
     * @return bool
     */
    public function delete(Data\User $user): bool
    {
        $result = $this->getDatabase()->query(
            "DELETE FROM `" . $this->getTableName() . "` WHERE id = ?",
            [$user->getId()],
            'i'
        );

        return $result->affected_rows > 0;
    }

    /**
     * @param Data\Team $team
     */
    public function save(Data\User $user): void
    {
        $db = $this->getDatabase();
        $params = [
            $user->getUsername(),
            $user->getPassword(),
            $user->getEmailAddress(),
            $user->getRole(),
        ];

        $types = 'ssss';
        if ($user->getId() === null || $user->getId() === 0) {
            $sql = "INSERT INTO `" . $this->getTableName() . "` (`username`, `password`, `emailAddress`, `role`) VALUES ( ?, ?, ?, ? )";
            $result = $db->query($sql, $params, $types);
            $user->setId($result->insert_id);
        } else {
            $sql = "UPDATE `" . $this->getTableName() . "` SET `username` = ?, `password` = ?, `emailAddress` = ?, `role` = ? WHERE `id` = ?";
            $params[] = $user->getId();
            $types .= 'i';
            $db->query($sql, $params, $types);
        }
    }


    /**
     * @return Data\User[]
     */
    public function getAll(): array
    {
        $data = $this->getDatabase()->fetchAll(
            "SELECT * FROM `" . $this->getTableName() . "`"
        );

        $teams = [];
        foreach ($data as $user) {
            $teams[] = $this->bindSqlResult($user);
        }

        return $teams;
    }
}
