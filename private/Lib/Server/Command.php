<?php

namespace Lib\Server;

use vakata\websocket\Server;

/**
 * Interface Command
 * @package Lib\Server
 */
interface Command
{
    /**
     * @param array    $payload
     * @param resource $socket
     * @param Server   $server
     */
    public function __construct(array $payload, $socket, Server $server);

    /**
     *
     */
    public function execute(): void;
}
