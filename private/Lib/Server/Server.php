<?php

namespace Lib\Server;

use Lib\Core\Settings;
use Lib\Server\Command\Error;
use Lib\Server\Command\Migrate;

final class Server
{
    /**
     * @var string
     */
    private $hostname;

    /**
     * @var \vakata\websocket\Server
     */
    private $server;

    /**
     * @var ConnectionContract[]
     */
    private $clients = [];

    public function __construct(string $hostname)
    {
        list($host, $port) = explode(':', $hostname);
        $this->hostname = $hostname;
        $this->clients = [];

        $this->server = new \vakata\websocket\Server(
            'wss://0.0.0.0:' . $port,
            '/etc/letsencrypt/live/geohunt.jmul.net-0001/full.pem'
        );

        $this->server->onConnect([$this, 'onConnect']);
        $this->server->onDisconnect([$this, 'onDisconnect']);
        $this->server->onMessage([$this, 'onMessage']);
    }

    /**
     *
     */
    public function run(): void
    {
        $this->server->run();
    }

    /**
     * @param array $conn
     */
    public function onConnect(array $conn): void
    {
        $this->log($conn['socket'], "CON", "Total clients: " . count($this->server->getClients()));
    }

    /**
     * @param array $recv
     * @param string $msg
     */
    public function onMessage(array $recv, string $msg): void
    {
        $message = json_decode($msg, true);

        $this->log($recv['socket'], "REC", $msg);

        try {
            $command = CommandFactory::getInstance()->getCommandObjectForMessage(
                $message['command'] ?? '',
                $message['payload'] ?? [],
                $recv['socket'],
                $this->server
            );

            if ($command === null) {
                return;
            }

            $command->execute();
        } catch (\Throwable $exception) {
            $command = new Error([
                'message' => $exception->getMessage(),
            ], $recv['socket'], $this->server);

            $command->execute();
        }
    }

    /**
     * @param array $conn
     */
    public function onDisconnect(array $conn): void
    {
        AuthFactory::getInstance()->deAuthSocket($conn['socket']);
        $this->log($conn['socket'], "CLS", "Total Clients: " . count($this->server->getClients()));
    }

    /**
     * @throws \Exception
     */
    public function close(): void
    {
        $this->server->shouldRun = false;
    }

    /**
     * @param resource $socket
     * @param string $tag
     * @param string $msg
     */
    private function log($socket, string $tag, string $msg): void
    {
        echo "[" . date('Y-m-d H:i:s') . "] [" . ((int)$socket) . "] [{$tag}] {$msg}" . PHP_EOL;
    }

    /**
     * @return string
     */
    public function getHostname(): string
    {
        return $this->hostname;
    }
}
