<?php

namespace Lib\Server;

use Lib\Core\Singleton;
use Lib\Server\Command\Admin;
use Lib\Server\Command\Answer;
use Lib\Server\Command\Auth;
use Lib\Server\Command\Error;
use Lib\Server\Command\Ping;
use Lib\Server\Command\Purchase;
use Lib\Server\Command\Update;

final class CommandFactory extends Singleton
{
    /**
     * @param string $command
     * @param array $payload
     *
     * @return Command
     */
    public function getCommandObjectForMessage(string $command, array $payload, $socket, \vakata\websocket\Server $server): ?Command
    {
        $commandClass = false;

        switch ($command) {
            /**
             * USER COMMANDS
             */
            case Error::COMMAND:
                $commandClass = Error::class;
                break;
            case Update::COMMAND:
                $commandClass = Update::class;
                break;
            case Auth::COMMAND:
                $commandClass = Auth::class;
                break;
            case Ping::COMMAND:
                $commandClass = Ping::class;
                break;
            case Purchase::COMMAND:
                $commandClass = Purchase::class;
                break;
            case Answer::COMMAND:
                $commandClass = Answer::class;
                break;

            /**
             * ADMIN COMMANDS
             */
            case Admin\Auth::COMMAND:
                $commandClass = Admin\Auth::class;
                break;
            case Admin\Answer\Correct::COMMAND:
                $commandClass = Admin\Answer\Correct::class;
                break;
            case Admin\Answer\Wrong::COMMAND:
                $commandClass = Admin\Answer\Wrong::class;
                break;
            case Admin\Answer\Remove::COMMAND:
                $commandClass = Admin\Answer\Remove::class;
                break;
            case Admin\MessageToTeam::COMMAND:
                $commandClass = Admin\MessageToTeam::class;
                break;
        }

        if ($commandClass === false) {
            $server->send($socket, json_encode(['error' => "Invalid Command send: {$command}"]));
            return null;
        }

        $commandObject = new $commandClass($payload, $socket, $server);

        if (!($commandObject instanceof Command)) {
            return null;
        }

        return $commandObject;
    }
}
