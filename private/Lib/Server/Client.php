<?php

namespace Lib\Server;

use Lib\Core\Settings;
use Lib\Core\Singleton;
use Lib\Exception\Server\IoException;
use Lib\Server\Command\Admin\Auth;

final class Client extends Singleton
{
    /**
     * @var resource
     */
    private $client;

    /**
     * Client constructor.
     * @param string $server
     * @throws IoException
     * @throws \vakata\websocket\WebSocketException
     */
    public function __construct(string $server = '')
    {
        if ($server === '') {
            $servers = Settings::getInstance()->get(['ws', 'servers']);
            $server = $servers[rand(0, count($servers)-1)];
        }

        $this->client = new \vakata\websocket\Client(
            'wss://' . $server
        );

        $this->writeCommand(Auth::COMMAND, [
            'passphrase' => Settings::getInstance()->get(['ws', 'adminPassphrase'])
        ]);
    }

    /**
     * @param string $command
     * @param array $payload
     *
     * @throws IoException
     */
    public function writeCommand(string $command, array $payload): void
    {
        $message = json_encode([
            'command' => $command,
            'payload' => $payload,
        ]);

        $send = $this->client->send($message);

        if (!$send) {
            $this->socketError("Error while sending message: {$message}");
        }
    }

    /**
     * @param string $message
     *
     * @throws IoException
     */
    private function socketError(string $message)
    {
        $errorcode = socket_last_error();
        $errormsg = socket_strerror($errorcode);

        throw new IoException("{$message} [{$errorcode}] {$errormsg}", $errorcode);
    }
}
