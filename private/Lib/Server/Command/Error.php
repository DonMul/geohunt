<?php

namespace Lib\Server\Command;

use Exception;
use Lib\Core\ExceptionHandler;
use Lib\Server\BaseCommand;
use Lib\Server\Command;

/**
 * Class Error
 * @package Lib\Server\Command
 */
final class Error extends BaseCommand implements Command
{
    const COMMAND = 'ERROR';

    /**
     *
     */
    public function execute(): void
    {
        $exception = new Exception($this->getValueFromPayload('message', ''));
        ExceptionHandler\Factory::getExceptionHandler()->handleException($exception);

        $this->sendCommandBack(self::COMMAND, [
            'message' => $exception->getMessage()
        ]);
    }
}
