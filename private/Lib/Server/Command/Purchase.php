<?php

namespace Lib\Server\Command;

use Lib\Core\Translation;
use Lib\Data\BuyableObject;
use Lib\Exception\ObjectNotFoundException;
use Lib\Exception\UserException;
use Lib\Server\BaseCommand;
use Lib\Server\Command;

/**
 * Class Purchase
 * @package Lib\Server\Command
 */
final class Purchase extends BaseCommand implements Command
{
    const COMMAND = 'PURCHASE';

    /**
     * @throws ObjectNotFoundException
     * @throws UserException
     * @throws \Lib\Exception\Server\UnautherizedException
     */
    public function execute(): void
    {
        $team = $this->getAuthorizedTeam();
        $objectId = $this->getValueFromPayload('objectId');
        $object = $this->getObjectRepo()->getById($objectId);
        if (!($object instanceof BuyableObject)) {
            throw new ObjectNotFoundException('Object', 'id', $objectId);
        }

        if ($team->getPoints() < $object->getPurchaseValue()) {
            throw new UserException(Translation::getInstance()->translate('error.notEnoughCurrency'));
        }

        $team->setPoints($team->getPoints() - $object->getPurchaseValue());
        $this->getTeamRepo()->save($team);

        $data = $this->getObjectRepo()->getObjectForTeam($team, $object);
        if (!$data) {
            $this->getObjectRepo()->addObjectToTeam($team, $object);
        } else {
            $this->getObjectRepo()->increaseTeamObjectAmount($team, $object);
        }

        $this->sendUpdate();
    }
}
