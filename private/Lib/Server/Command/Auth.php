<?php

namespace Lib\Server\Command;

use Lib\Server\BaseCommand;
use Lib\Server\Command;

/**
 * Class Auth
 * @package Lib\Server\Command
 */
final class Auth extends BaseCommand implements Command
{
    const COMMAND = 'AUTH';

    /**
     * @throws \Lib\Exception\Server\UnautherizedException
     */
    public function execute(): void
    {
        $this->authorizeTeam($this->getValueFromPayload('teamCode'));
        $this->sendCommandBack('AUTH SUCCESS', []);
    }
}
