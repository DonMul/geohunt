<?php

namespace Lib\Server\Command;

use Lib\Core\Translation;
use Lib\Data\Question;
use Lib\Exception\ObjectNotFoundException;
use Lib\Exception\UserException;
use Lib\Server\BaseCommand;
use Lib\Server\Command;

/**
 * Class Auth
 * @package Lib\Server\Command
 */
final class Answer extends BaseCommand implements Command
{
    const COMMAND = 'ANSWER';

    /**
     * @throws UserException
     *
     * @throws \Lib\Exception\Server\UnautherizedException
     */
    public function execute(): void
    {
        $questionId = $this->getValueFromPayload('questionId', 0);
        $question = $this->getQuestionRepo()->getById($questionId);
        if (!($question instanceof Question)) {
            throw new ObjectNotFoundException('Question', 'id', $questionId);
        }

        switch ($question->getType()) {
            case Question::TYPE_MULTIPLE:
                $this->checkMultipleQuestion($question);
                break;
            default:
                $this->checkOpenQuestion($question);
                break;
        }

        $this->sendUpdate();
    }

    /**
     * @param Question $question
     * @throws UserException
     * @throws \Lib\Exception\Server\UnautherizedException
     */
    public function checkOpenQuestion(Question $question)
    {
        $team = $this->getAuthorizedTeam();
        $answer = $this->getValueFromPayload('answer', 0);

        if ($this->getAnswerRepo()->ensureQuestionAnswered($team, $question, $answer, false, false) !== true) {
            throw new UserException(Translation::getInstance()->translate('error.alreadyAnswered'));
        }

        $this->sendCommandBack(Message::COMMAND, [
            'message' => Translation::getInstance()->translate('question.underReview'),
        ]);
    }

    /**
     * @param Question $question
     * @throws UserException
     * @throws \Lib\Exception\Server\UnautherizedException
     */
    public function checkMultipleQuestion(Question $question)
    {
        $translation = Translation::getInstance();

        $team = $this->getAuthorizedTeam();
        $answerId = $this->getValueFromPayload('answer', 0);

        $answer = $this->getAnswerRepo()->getById($answerId);
        $allAnswers = $this->getAnswerRepo()->findByQuestionId($question->getId());

        $isCorrect = false;
        $correctAnswer = null;

        foreach ($allAnswers as $possibleAnswer) {
            if ($possibleAnswer->isCorrect() == true) {
                $correctAnswer = $possibleAnswer;
                $isCorrect = ($possibleAnswer->getId() == $answerId);
                break;
            }
        }

        if ($this->getAnswerRepo()->ensureQuestionAnswered($team, $question, $answer->getAnswer(), $isCorrect, true) !== true) {
            throw new UserException(
                $translation->translate(
                    'error.alreadyAnswered'
                )
            );
        }

        if ($isCorrect === false) {
            throw new UserException(
                $translation->translate(
                    'question.wrongAnswer',
                    [
                        'correctAnswer' => $correctAnswer->getAnswer()
                    ]
                )
            );
        }

        $team->setPoints($team->getPoints() + $question->getValue());
        $this->getTeamRepo()->save($team);

        $this->sendCommandBack(Message::COMMAND, [
            'message' => $translation->translate("question.correctAnswer"),
        ]);
    }
}
