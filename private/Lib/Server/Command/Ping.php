<?php

namespace Lib\Server\Command;

use Lib\Server\BaseCommand;
use Lib\Server\Command;

/**
 * Class Ping
 * @package Lib\Server\Command
 */
final class Ping extends BaseCommand implements Command
{
    const COMMAND = 'PING';

    /**
     * @throws \Lib\Exception\Server\UnautherizedException
     */
    public function execute(): void
    {
        $team = $this->getAuthorizedTeam();

        $team->setLon(
            $this->getValueFromPayload('lon')
        );

        $team->setLat(
            $this->getValueFromPayload('lat')
        );

        $this->getTeamRepo()->save($team);
        $this->sendUpdate();
    }
}
