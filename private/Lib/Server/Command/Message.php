<?php

namespace Lib\Server\Command;

use Lib\Server\BaseCommand;
use Lib\Server\Command;

/**
 * Class Message
 * @package Lib\Server\Command
 */
final class Message extends BaseCommand implements Command
{
    const COMMAND = 'MESSAGE';

    /**
     *
     */
    public function execute(): void
    {
    }
}
