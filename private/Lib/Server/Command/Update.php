<?php

namespace Lib\Server\Command;

use Lib\Server\BaseCommand;
use Lib\Server\Command;

/**
 * Class Update
 * @package Lib\Server\Command
 */
final class Update extends BaseCommand implements Command
{
    const COMMAND = 'UPDATE';

    /**
     * @throws \Lib\Exception\Server\UnautherizedException
     */
    public function execute(): void
    {
        $this->sendUpdate();
    }
}
