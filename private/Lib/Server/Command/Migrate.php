<?php

namespace Lib\Server\Command;

use Lib\Server\BaseCommand;
use Lib\Server\Command;

/**
 * Class Migrate
 * @package Lib\Server\Command
 */
final class Migrate extends BaseCommand implements Command
{
    const COMMAND = 'MIGRATE';

    /**
     *
     */
    public function execute(): void
    {
        $this->sendCommandBack(self::COMMAND, [
            'server' => $this->getValueFromPayload('server')
        ]);
    }
}
