<?php

namespace Lib\Server\Command\Admin\Answer;

use Lib\Core\Translation;
use Lib\Server\BaseCommand;
use Lib\Server\Command;

/**
 * Class Remove
 * @package Lib\Server\Command\Admin\Answer
 */
final class Remove extends BaseCommand implements Command
{
    const COMMAND = 'ADMIN_ANSWER_REMOVE';

    /**
     * @throws \Lib\Exception\Server\UnautherizedException
     */
    public function execute(): void
    {
        $this->ensureAdminAuthorized();

        $teamId = $this->getValueFromPayload('teamId');
        $questionId = $this->getValueFromPayload('questionId');

        $team = $this->getTeamRepo()->getById($teamId);
        $question = $this->getQuestionRepo()->getById($questionId);

        $this->getAnswerRepo()->removeAnswerForTeam($team, $question);

        $this->sendMessageToTeam($team, Translation::getInstance()->translate(
            'question.answer.removed',
            [
                'title' => $question->getTitle()
            ]
        ), true);
    }
}
