<?php

namespace Lib\Server\Command\Admin\Answer;

use Lib\Core\Translation;
use Lib\Data\TeamAnswer;
use Lib\Server\BaseCommand;
use Lib\Server\Command;

/**
 * Class Correct
 * @package Lib\Server\Command\Admin\Answer
 */
final class Correct extends BaseCommand implements Command
{
    const COMMAND = 'ADMIN_ANSWER_CORRECT';

    /**
     * @throws \Lib\Exception\Server\UnautherizedException
     */
    public function execute(): void
    {
        $this->ensureAdminAuthorized();

        $teamId = $this->getValueFromPayload('teamId');
        $questionId = $this->getValueFromPayload('questionId');

        $team = $this->getTeamRepo()->getById($teamId);
        $question = $this->getQuestionRepo()->getById($questionId);

        $teamQuestionAnswer = $this->getAnswerRepo()->getAnserToQuestionByTeam($team, $question);
        $answer = '';
        if ($teamQuestionAnswer instanceof TeamAnswer) {
            $answer = $teamQuestionAnswer->getAnswer();
        }

        if (!($teamQuestionAnswer instanceof TeamAnswer) || $teamQuestionAnswer->isCorrect() === false) {
            $team->setPoints($team->getPoints() + $question->getValue());
            $this->getTeamRepo()->save($team);
        }

        $this->getAnswerRepo()->removeAnswerForTeam($team, $question);
        $this->getAnswerRepo()->ensureQuestionAnswered($team, $question, $answer, true, true);

        $this->sendMessageToTeam($team, Translation::getInstance()->translate(
            'question.answer.correct',
            [
                'title' => $question->getTitle()
            ]
        ), true);
    }
}
