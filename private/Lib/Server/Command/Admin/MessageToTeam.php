<?php

namespace Lib\Server\Command\Admin;

use Lib\Server\BaseCommand;
use Lib\Server\Command;

/**
 * Class Message
 * @package Lib\Server\Command
 */
final class MessageToTeam extends BaseCommand implements Command
{
    const COMMAND = 'MESSAGE_TO_TEAM';

    /**
     * @var bool
     */
    private $shouldGossip = false;

    /**
     * @throws \Lib\Exception\Server\UnautherizedException
     */
    public function execute(): void
    {
        $this->ensureAdminAuthorized();

        $this->sendMessageToTeam(
            $this->getTeamRepo()->getById($this->getValueFromPayload('teamId')),
            $this->getValueFromPayload('message'),
            $this->shouldGossip
        );
    }

    /**
     * @param bool $shouldGossip
     */
    final public function shouldGossip(bool $shouldGossip)
    {
        $this->shouldGossip = $shouldGossip;
    }
}
