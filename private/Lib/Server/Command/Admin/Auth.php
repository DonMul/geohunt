<?php

namespace Lib\Server\Command\Admin;

use Lib\Server\BaseCommand;
use Lib\Server\Command;

/**
 * Class Auth
 * @package Lib\Server\Command\Admin
 */
final class Auth extends BaseCommand implements Command
{
    const COMMAND = 'ADMIN_AUTH';

    /**
     * @throws \Lib\Exception\Server\UnautherizedException
     */
    public function execute(): void
    {
        $passphrase = $this->getValueFromPayload('passphrase');
        $this->authorizeAdmin($passphrase);
        $this->sendCommandBack('AUTH SUCCESS', []);
    }
}
