<?php


namespace Lib\Server;

use Lib\Core\RepositoryContainer;
use Lib\Core\Settings;
use Lib\Core\Util;
use Lib\Data\Team;
use Lib\Server\Command\Admin\MessageToTeam;
use Lib\Server\Command\Message;
use Lib\Server\Command\Update;
use vakata\websocket\Server;
use function json_encode;

abstract class BaseCommand extends RepositoryContainer implements Command
{
    /**
     * @var int[]
     */
    private static $lockedTeamIds = [];

    /**
     * @var array
     */
    private $payload;

    /**
     * @var resource
     */
    private $socket;

    /**
     * @var Server
     */
    private $server;

    /**
     * @param array    $payload
     * @param resource $socket
     * @param Server   $server
     */
    final public function __construct(array $payload, $socket, Server $server)
    {
        $this->payload = $payload;
        $this->socket = $socket;
        $this->server = $server;
    }

    /**
     * @param string|string[] $key
     * @param mixed           $default
     *
     * @return mixed
     */
    protected function getValueFromPayload($key, $default = null)
    {
        return Util::arrayGet($this->payload, $key, $default);
    }

    /**
     * @param string $command
     * @param array  $payload
     *
     * @return bool
     */
    protected function sendCommandBack(string $command, array $payload): bool
    {
        $this->log("SND", "Command: {$command}, payload: " . json_encode($payload));
        return $this->server->send($this->socket, json_encode([
            'command' => $command,
            'payload' => $payload
        ]));
    }

    /**
     * @return \Lib\Data\Team
     *
     * @throws \Lib\Exception\Server\UnautherizedException
     */
    protected function getAuthorizedTeam(): Team
    {
        return AuthFactory::getInstance()->getTeamForSocket($this->socket);
    }

    /**
     * @param string $teamCode
     *
     * @throws \Lib\Exception\Server\UnautherizedException
     */
    protected function authorizeTeam(string $teamCode): void
    {
        AuthFactory::getInstance()->authorizeSocket($this->socket, $teamCode);
    }

    /**
     * @param string $passphrase
     *
     * @throws \Lib\Exception\Server\UnautherizedException
     */
    protected function authorizeAdmin(string $passphrase): void
    {
        AuthFactory::getInstance()->authorizeSocketForAdmin($this->socket, $passphrase);
    }

    /**
     * @throws \Lib\Exception\Server\UnautherizedException
     */
    protected function ensureAdminAuthorized(): void
    {
        AuthFactory::getInstance()->ensureAdminAuthorized($this->socket);
    }

    /**
     * @throws \Lib\Exception\Server\UnautherizedException
     */
    protected function sendUpdate()
    {
        $this->sendCommandBack(Update::COMMAND, [
            'objects'   => array_map(function (&$entry) {
                return [
                    'id' => $entry['id'],
                    'amount' => $entry['amount']
                ];
            }, $this->getObjectRepo()->getAllByTeam($this->getAuthorizedTeam())),
            'points'    => $this->getAuthorizedTeam()->getPoints(),
        ]);
    }

    /**
     * @param string $tag
     * @param string $msg
     */
    protected function log(string $tag, string $msg): void
    {
        echo "[" . date('Y-m-d H:i:s') . "] [" . ((int)$this->socket) . "] [{$tag}] {$msg}" . PHP_EOL;
    }

    /**
     * @param Team $team
     * @param string $message
     * @param bool $gossip
     */
    protected function sendMessageToTeam(Team $team, $message, $gossip = false)
    {
        $sockets = AuthFactory::getInstance()->getSocketsForTeam($team);
        foreach ($sockets as $socket) {
            $this->server->send($socket, json_encode([
                'command' => Message::COMMAND,
                'payload' => [
                    'message' => $message
                ],
            ]));
        }

        if ($gossip === true) {
            foreach (Settings::getInstance()->get(['ws', 'servers']) as $server) {
                if ($server == Settings::getInstance()->get('currentServer')) {
                    continue;
                }
                $client = new Client($server);
                $client->writeCommand(MessageToTeam::COMMAND, [
                    'message' => $message,
                    'teamId' => $team->getId(),
                ]);
            }
        }
    }
}
