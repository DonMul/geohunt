<?php

namespace Lib\Server;

use Lib\Core\Settings;
use Lib\Core\Singleton;
use Lib\Core\Util;
use Lib\Data\Team;
use Lib\Exception\Server\UnautherizedException;

/**
 * Class AuthFactory
 * @package Lib\Server
 */
final class AuthFactory extends Singleton
{
    /**
     * @var \Lib\Repository\Team
     */
    private $teamRepo;

    /**
     * @var int[]
     */
    private $socketTeamMapping = [];

    /**
     * @var int[]
     */
    private $socketAdminMapping = [];

    /**
     * @return \Lib\Repository\Team
     */
    private function getTeamRepository(): \Lib\Repository\Team
    {
        if (!($this->teamRepo instanceof \Lib\Repository\Team)) {
            $this->teamRepo = new \Lib\Repository\Team();
        }

        return $this->teamRepo;
    }

    /**
     * @param resource $socket
     * @return Team
     * @throws UnautherizedException
     */
    public function getTeamForSocket($socket): Team
    {
        if (!isset($this->socketTeamMapping[(int)$socket])) {
            throw new UnautherizedException();
        }

        return $this->getTeamRepository()->getById($this->socketTeamMapping[(int)$socket]['teamId']);
    }

    /**
     * @param Team $team
     * @return \Generator
     */
    public function getSocketsForTeam(Team $team): \Generator
    {
        foreach ($this->socketTeamMapping as $socket => $data) {
            if ($data['teamId'] === $team->getId()) {
                yield $data['socket'];
            }
        }
    }

    /**
     * @param resource $socket
     */
    public function deAuthSocket($socket): void
    {
        if (isset($this->socketTeamMapping[(int)$socket])) {
            unset($this->socketTeamMapping[(int)$socket]);
        }

        if (isset($this->socketAdminMapping[(int)$socket])) {
            unset($this->socketAdminMapping[(int)$socket]);
        }
    }

    /**
     * @param resource $socket
     * @param string $teamCode
     *
     * @throws UnautherizedException
     */
    public function authorizeSocket($socket, string $teamCode): void
    {
        $team = $this->getTeamRepository()->getByCode($teamCode);
        if (!($team instanceof Team)) {
            throw new UnautherizedException();
        }

        $this->socketTeamMapping[(int)$socket] = ['teamId' => $team->getId(), 'socket' => $socket];
        $this->socketAdminMapping[(int)$socket] = false;
    }

    /**
     * @param resource $socket
     * @param string $passphrase
     * @throws UnautherizedException
     */
    public function authorizeSocketForAdmin($socket, string $passphrase): void
    {
        if ($passphrase !== Settings::getInstance()->get(['ws', 'adminPassphrase'])) {
            throw new UnautherizedException();
        }

        $this->socketAdminMapping[(int)$socket] = true;
    }

    /**
     * @param resource $socket
     * @throws UnautherizedException
     */
    public function ensureAdminAuthorized($socket): void
    {
        if (Util::arrayGet($this->socketAdminMapping, (int)$socket, false) !== true) {
            throw new UnautherizedException();
        }
    }
}
