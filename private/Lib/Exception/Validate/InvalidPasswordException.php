<?php

namespace Lib\Exception\Validate;

use Lib\Core\Translation;
use Lib\Exception\UserException;

/**
 * Class InvalidPasswordException
 * @package Lib\Exception\User
 */
final class InvalidPasswordException extends UserException
{
    /**
     * InvalidPasswordException constructor.
     */
    public function __construct()
    {
        parent::__construct(Translation::getInstance()->translate('error.validate.password'));
    }
}
