<?php

namespace Lib\Exception\Validate;

use Lib\Core\Translation;
use Lib\Exception\UserException;

/**
 * Class InvalidEmailAddressException
 * @package Lib\Exception\User
 */
final class InvalidEmailAddressException extends UserException
{
    /**
     * InvalidEmailAddressException constructor.
     * @param string $emailAddress
     */
    public function __construct(string $emailAddress)
    {
        parent::__construct(Translation::getInstance()->translate('error.validate.emailAddress', ['email' => $emailAddress]));
    }
}
