<?php

namespace Lib\Exception\Validate;

use Lib\Core\Translation;
use Lib\Exception\UserException;

/**
 * Class MinValueException
 * @package Lib\Exception\Validate
 */
final class MinValueException extends UserException
{
    /**
     * UsernameAlreadyTakenException constructor.
     * @param string $username
     */
    public function __construct(string $field, int $minLength)
    {
        parent::__construct(Translation::getInstance()->translate('error.validate.minValue', ['field' => $field, 'minValue' => $minLength]));
    }
}
