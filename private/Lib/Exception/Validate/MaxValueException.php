<?php

namespace Lib\Exception\Validate;

use Lib\Core\Translation;
use Lib\Exception\UserException;

/**
 * Class MinValueException
 * @package Lib\Exception\Validate
 */
final class MaxValueException extends UserException
{
    /**
     * MaxValueException constructor.
     * @param string $field
     * @param int $maxValue
     */
    public function __construct(string $field, int $maxValue)
    {
        parent::__construct(Translation::getInstance()->translate('error.validate.maxValue', ['field' => $field, 'maxValue' => $maxValue]));
    }
}
