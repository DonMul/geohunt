<?php

namespace Lib\Exception\Validate;

use Lib\Core\Translation;
use Lib\Exception\UserException;

/**
 * Class MinLengthException
 * @package Lib\Exception\Validate
 */
final class MinLengthException extends UserException
{
    /**
     * UsernameAlreadyTakenException constructor.
     * @param string $username
     */
    public function __construct(string $field, int $minLength)
    {
        parent::__construct(Translation::getInstance()->translate('error.validate.minLength', ['field' => $field, 'minLength' => $minLength]));
    }
}
