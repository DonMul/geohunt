<?php

namespace Lib\Exception\Validate;

use Lib\Core\Translation;
use Lib\Exception\UserException;

/**
 * Class ValidDateInFutureException
 * @package Lib\Exception\User
 */
final class ValidDateInFutureException extends UserException
{
    /**
     * InvalidEmailAddressException constructor.
     * @param string $emailAddress
     */
    public function __construct(string $field)
    {
        parent::__construct(Translation::getInstance()->translate('error.validate.dateInFuture', ['field' => $field]));
    }
}
