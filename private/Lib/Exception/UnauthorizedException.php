<?php


namespace Lib\Exception;

use Lib\Core\Translation;

/**
 * Class UnauthorizedException
 * @package Lib\Exception
 */
final class UnauthorizedException extends UserException
{
    /**
     * UnauthorizedException constructor.
     */
    public function __construct()
    {
        parent::__construct(Translation::getInstance()->translate('error.unautherized'));
    }
}
