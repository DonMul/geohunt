<?php

namespace Lib\Exception\Server;

/**
 * Class UnableToConnectException
 * @package Lib\Exception\Server
 */
final class UnableToConnectException extends \Exception
{
    public function __construct(string $server)
    {
        parent::__construct("Could not connect to server {$server}");
    }
}
