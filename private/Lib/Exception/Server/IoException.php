<?php

namespace Lib\Exception\Server;

/**
 * Class UnableToConnectException
 * @package Lib\Exception\Server
 */
final class IoException extends \Exception
{
    public function __construct(string $message, int $code)
    {
        parent::__construct($message, $code);
    }
}
