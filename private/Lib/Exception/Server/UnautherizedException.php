<?php

namespace Lib\Exception\Server;

use Lib\Core\Translation;
use Lib\Exception\UserException;

/**
 * Class InvalidCodeException
 * @package Lib\Exception
 */
final class UnautherizedException extends UserException
{
    /**
     * UnauthorizedException constructor.
     */
    public function __construct()
    {
        parent::__construct(Translation::getInstance()->translate('error.unautherized'));
    }
}
