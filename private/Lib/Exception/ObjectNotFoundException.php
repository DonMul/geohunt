<?php


namespace Lib\Exception;

use Lib\Core\Translation;

/**
 * Class ObjectNotFoundException
 * @package Lib\Exception
 */
final class ObjectNotFoundException extends UserException
{
    /**
     * ObjectNotFoundException constructor.
     * @param string $object
     * @param string $identifyingField
     * @param string $identifier
     */
    public function __construct(string $object, string $identifyingField, string $identifier)
    {
        parent::__construct(Translation::getInstance()->translate('error.objectNotFound'), [
            'object' => $object,
            'field' => $identifyingField,
            'identifier' => $identifier
        ]);
    }
}
