<?php


namespace Lib\Exception;

use Lib\Core\Translation;

/**
 * Class InvalidCodeException
 * @package Lib\Exception
 */
final class InvalidCodeException extends UserException
{
    /**
     * InvalidCodeException constructor.
     */
    public function __construct()
    {
        parent::__construct(Translation::getInstance()->translate('error.invalidCode'));
    }
}
