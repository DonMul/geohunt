<?php

namespace Lib\Exception\User;

use Lib\Core\Translation;
use Lib\Exception\UserException;

/**
 * Class InvalidUsernameOrPasswordException
 * @package Lib\Exception\User
 */
final class InvalidUsernameOrPasswordException extends UserException
{
    /**
     * InvalidEmailAddressException constructor.
     */
    public function __construct()
    {
        parent::__construct(Translation::getInstance()->translate('error.invalidUsernameOrPassword'));
    }
}
