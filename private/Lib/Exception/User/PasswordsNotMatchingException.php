<?php

namespace Lib\Exception\User;

use Lib\Core\Translation;
use Lib\Exception\UserException;

/**
 * Class PasswordsNotMatchingException
 * @package Lib\Exception\User
 */
final class PasswordsNotMatchingException extends UserException
{
    /**
     * InvalidPasswordException constructor.
     */
    public function __construct()
    {
        parent::__construct(Translation::getInstance()->translate('error.mismatchPassword'));
    }
}
