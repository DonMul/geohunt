<?php

namespace Lib\Exception\User;

use Lib\Core\Translation;
use Lib\Exception\UserException;

/**
 * Class UsernameAlreadyTakenException
 * @package Lib\Exception
 */
final class UsernameAlreadyTakenException extends UserException
{
    /**
     * UsernameAlreadyTakenException constructor.
     * @param string $username
     */
    public function __construct(string $username)
    {
        parent::__construct(Translation::getInstance()->translate('error.usernameAlreadyTaken', ['username' => $username]));
    }
}
