<?php

namespace Lib\Exception;

use Throwable;

/**
 * Class UserException
 * @package Lib\Exception
 */
class UserException extends \Exception
{
    /**
     * UserException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
