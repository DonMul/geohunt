<?php
namespace Lib\Data;

final class TeamAnswer
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $teamId;

    /**
     * @var int
     */
    private $questionId;

    /**
     * @var string
     */
    private $answer;

    /**
     * @var string
     */
    private $answerDate;

    /**
     * @var bool
     */
    private $isHandled;

    /**
     * @var bool
     */
    private $isCorrect;

    /**
     * TeamAnswer constructor.
     * @param int $id
     * @param int $teamId
     * @param int $questionId
     * @param string $answer
     * @param string $answerDate
     * @param bool $isHandled
     * @param bool $isCorrect
     */
    public function __construct(int $id, int $teamId, int $questionId, string $answer, string $answerDate, bool $isHandled, bool $isCorrect)
    {
        $this->setId($id);
        $this->setTeamId($teamId);
        $this->setQuestionId($questionId);
        $this->setAnswer($answer);
        $this->setAnswerDate($answerDate);
        $this->setIsHandled($isHandled);
        $this->setIsCorrect($isCorrect);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getTeamId(): int
    {
        return $this->teamId;
    }

    /**
     * @param int $teamId
     */
    public function setTeamId(int $teamId): void
    {
        $this->teamId = $teamId;
    }

    /**
     * @return int
     */
    public function getQuestionId(): int
    {
        return $this->questionId;
    }

    /**
     * @param int $questionId
     */
    public function setQuestionId(int $questionId): void
    {
        $this->questionId = $questionId;
    }

    /**
     * @return string
     */
    public function getAnswer(): string
    {
        return $this->answer;
    }

    /**
     * @param string $answer
     */
    public function setAnswer(string $answer): void
    {
        $this->answer = $answer;
    }

    /**
     * @return string
     */
    public function getAnswerDate(): string
    {
        return $this->answerDate;
    }

    /**
     * @param string $answerDate
     */
    public function setAnswerDate(string $answerDate): void
    {
        $this->answerDate = $answerDate;
    }

    /**
     * @return bool
     */
    public function isHandled(): bool
    {
        return $this->isHandled;
    }

    /**
     * @param bool $isHandled
     */
    public function setIsHandled(bool $isHandled): void
    {
        $this->isHandled = $isHandled;
    }

    /**
     * @return bool
     */
    public function isCorrect(): bool
    {
        return $this->isCorrect;
    }

    /**
     * @param bool $isCorrect
     */
    public function setIsCorrect(bool $isCorrect): void
    {
        $this->isCorrect = $isCorrect;
    }
}
