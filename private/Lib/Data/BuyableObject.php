<?php

namespace Lib\Data;

/**
 * Class BuyableObject
 * @package Lib\Data
 */
final class BuyableObject
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $image;

    /**
     * @var int
     */
    private $purchaseValue;

    /**
     * @var int
     */
    private $points;

    /**
     * @var int
     */
    private $gameId;

    /**
     * Object constructor.
     * @param int $id
     * @param string $name
     * @param string $image
     * @param int $purchaseValue
     * @param int $points
     */
    public function __construct(int $id, string $name, string $image, int $purchaseValue, int $points, int $gameId)
    {
        $this->setId($id);
        $this->setName($name);
        $this->setImage($image);
        $this->setPurchaseValue($purchaseValue);
        $this->setPoints($points);
        $this->setGameId($gameId);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage(string $image): void
    {
        $this->image = $image;
    }

    /**
     * @return int
     */
    public function getPurchaseValue(): int
    {
        return $this->purchaseValue;
    }

    /**
     * @param int $purchaseValue
     */
    public function setPurchaseValue(int $purchaseValue): void
    {
        $this->purchaseValue = $purchaseValue;
    }

    /**
     * @return int
     */
    public function getPoints(): int
    {
        return $this->points;
    }

    /**
     * @param int $points
     */
    public function setPoints(int $points): void
    {
        $this->points = $points;
    }

    /**
     * @return int
     */
    public function getGameId(): int
    {
        return $this->gameId;
    }

    /**
     * @param int $gameId
     */
    public function setGameId(int $gameId): void
    {
        $this->gameId = $gameId;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'image' => $this->getImage(),
            'purchaseValue' => $this->getPurchaseValue(),
            'points' => $this->getPoints(),
            'gameId' => $this->getGameId(),
        ];
    }
}
