<?php

namespace Lib\Data;

/**
 * Class Message
 * @package Lib\Data
 */
final class Message
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $teamId;

    /**
     * @var string
     */
    private $message;

    /**
     * @var string
     */
    private $creationDate;

    /**
     * Message constructor.
     * @param int $id
     * @param int $teamId
     * @param string $message
     * @param string $creationDate
     */
    public function __construct(int $id, int $teamId, string $message, string $creationDate)
    {
        $this->setId($id);
        $this->setTeamId($teamId);
        $this->setMessage($message);
        $this->setCreationDate($creationDate);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getTeamId(): int
    {
        return $this->teamId;
    }

    /**
     * @param int $teamId
     */
    public function setTeamId(int $teamId): void
    {
        $this->teamId = $teamId;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getCreationDate(): string
    {
        return $this->creationDate;
    }

    /**
     * @param string $creationDate
     */
    public function setCreationDate(string $creationDate): void
    {
        $this->creationDate = $creationDate;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'teamId' => $this->getTeamId(),
            'message' => $this->getMessage(),
            'creationDate' => $this->getCreationDate(),
        ];
    }
}
