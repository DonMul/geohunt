<?php

namespace Lib\Data;

/**
 * Class Profile
 * @package Lib\Data
 */
final class Profile
{
    const ROLE_USER = 'user';
    const ROLE_ADMIN = 'admin';

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $userId;

    /**
     * @var string
     */
    private $publicName;

    /**
     * @var string
     */
    private $profilePicture;

    /**
     * @var string
     */
    private $role;

    /**
     * User constructor.
     * @param int $id
     * @param int $userId
     * @param string $publicName
     * @param string $profilePicture
     * @param string $role
     */
    public function __construct(int $id, int $userId, string $publicName, string $profilePicture, string $role)
    {
        $this->setId($id);
        $this->setUserId($userId);
        $this->setPublicName($publicName);
        $this->setProfilePicture($profilePicture);
        $this->setRole($role);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getPublicName(): string
    {
        return $this->publicName;
    }

    /**
     * @param string $publicName
     */
    public function setPublicName(string $publicName): void
    {
        $this->publicName = $publicName;
    }

    /**
     * @return string
     */
    public function getProfilePicture(): string
    {
        return $this->profilePicture;
    }

    /**
     * @param string $profilePicture
     */
    public function setProfilePicture(string $profilePicture): void
    {
        $this->profilePicture = $profilePicture;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole(string $role): void
    {
        $this->role = $role;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'userId' => $this->getUserId(),
            'publicName' => $this->getPublicName(),
            'profilePicture' => $this->getProfilePicture(),
        ];
    }
}
