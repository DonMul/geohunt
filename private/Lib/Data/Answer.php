<?php

namespace Lib\Data;

/**
 * Class Answer
 * @package Lib\Data
 */
final class Answer
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $questionId;

    /**
     * @var int
     */
    private $position;

    /**
     * @var string
     */
    private $answer;

    /**
     * @var bool
     */
    private $isCorrect;

    /**
     * Answer constructor.
     * @param int $id
     * @param int $questionId
     * @param int $position
     * @param string $answer
     * @param bool $isCorrect
     */
    public function __construct(int $id, int $questionId, int $position, string $answer, bool $isCorrect)
    {
        $this->setId($id);
        $this->setQuestionId($questionId);
        $this->setPosition($position);
        $this->setAnswer($answer);
        $this->setIsCorrect($isCorrect);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getQuestionId(): int
    {
        return $this->questionId;
    }

    /**
     * @param int $questionId
     */
    public function setQuestionId(int $questionId): void
    {
        $this->questionId = $questionId;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition(int $position): void
    {
        $this->position = $position;
    }

    /**
     * @return string
     */
    public function getAnswer(): string
    {
        return $this->answer;
    }

    /**
     * @param string $answer
     */
    public function setAnswer(string $answer): void
    {
        $this->answer = $answer;
    }

    /**
     * @return bool
     */
    public function isCorrect(): bool
    {
        return $this->isCorrect;
    }

    /**
     * @param bool $isCorrect
     */
    public function setIsCorrect(bool $isCorrect): void
    {
        $this->isCorrect = $isCorrect;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'questionId' => $this->getQuestionId(),
            'position' => $this->getPosition(),
            'answer' => $this->getAnswer(),
        ];
    }
}
