<?php

namespace Lib\Data;

/**
 * Class User
 * @package Lib\Data
 */
final class User
{
    const ROLE_USER = 'user';
    const ROLE_ADMIN = 'admin';

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $emailAddress;

    /**
     * @var string
     */
    private $role;

    /**
     * User constructor.
     * @param int $id
     * @param string $username
     * @param string $password
     * @param string $emailAddress
     * @param string $role
     */
    public function __construct(int $id, string $username, string $password, string $emailAddress, string $role)
    {
        $this->setId($id);
        $this->setUsername($username);
        $this->setPassword($password);
        $this->setEmailAddress($emailAddress);
        $this->setRole($role);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getEmailAddress(): string
    {
        return $this->emailAddress;
    }

    /**
     * @param string $emailAddress
     */
    public function setEmailAddress(string $emailAddress): void
    {
        $this->emailAddress = $emailAddress;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole(string $role): void
    {
        $this->role = $role;
    }

    /**
     * @param string $password
     * @return string
     */
    public static function hashPassword($password)
    {
        return hash('sha512', $password);
    }
}
