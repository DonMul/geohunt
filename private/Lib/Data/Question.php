<?php

namespace Lib\Data;

/**
 * Class Question
 * @package Lib\Data
 */
final class Question
{
    const TYPE_MULTIPLE = 'multiple';
    const TYPE_TEXT = 'text';
    const TYPE_OTHER = 'other';

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $description;

    /**
     * @var int
     */
    private $value;

    /**
     * @var float
     */
    private $lat;

    /**
     * @var float
     */
    private $lon;

    /**
     * @var string
     */
    private $type;

    /**
     * @var int
     */
    private $gameId;

    /**
     * Question constructor.
     * @param int $id
     * @param string $title
     * @param string $description
     * @param int $value
     * @param int $lat
     * @param int $lon
     * @param string $type
     */
    public function __construct(int $id, string $title, string $description, int $value, float $lat, float $lon, string $type, int $gameId)
    {
        $this->setId($id);
        $this->setTitle($title);
        $this->setDescription($description);
        $this->setValue($value);
        $this->setLat($lat);
        $this->setLon($lon);
        $this->setType($type);
        $this->setGameId($gameId);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue(int $value): void
    {
        $this->value = $value;
    }

    /**
     * @return float
     */
    public function getLat(): float
    {
        return $this->lat;
    }

    /**
     * @param float $lat
     */
    public function setLat(float $lat): void
    {
        $this->lat = $lat;
    }

    /**
     * @return float
     */
    public function getLon(): float
    {
        return $this->lon;
    }

    /**
     * @param float $lon
     */
    public function setLon(float $lon): void
    {
        $this->lon = $lon;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getGameId(): int
    {
        return $this->gameId;
    }

    /**
     * @param int $gameId
     */
    public function setGameId(int $gameId): void
    {
        $this->gameId = $gameId;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'description' => $this->getDescription(),
            'value' => $this->getValue(),
            'lat' => $this->getLat(),
            'lon' => $this->getLon(),
            'type' => $this->getType(),
            'gameId' => $this->getGameId(),
        ];
    }

    /**
     * @return string[]
     */
    public static function getTypes(): array
    {
        return [
            self::TYPE_MULTIPLE,
            self::TYPE_OTHER,
            self::TYPE_TEXT
        ];
    }
}
