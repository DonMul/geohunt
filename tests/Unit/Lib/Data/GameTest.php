<?php

/**
 * Class GameTest
 */
final class GameTest extends \PHPUnit\Framework\TestCase
{
    /**
     *
     */
    public function test_getters_and_setters()
    {
        $id = rand(0, 10000);
        $userId = rand(0, 10000);
        $name = uniqid();
        $startTime = date('Y-m-d H:i:s');
        $endTime = date('Y-m-d H:i:s');

        $game = new \Lib\Data\Game(
            $id,
            $userId,
            $name,
            $startTime,
            $endTime
        );

        $this->assertEquals($id, $game->getId());
        $this->assertEquals($userId, $game->getUserId());
        $this->assertEquals($name, $game->getName());
        $this->assertEquals($startTime, $game->getStartTime());
        $this->assertEquals($endTime, $game->getEndTime());
    }
}