<?php

/**
 * Class ProfileTest
 */
final class ProfileTest extends \PHPUnit\Framework\TestCase
{
    /**
     *
     */
    public function test_getters_and_setters()
    {
        $id = rand(0, 10000);
        $userId = rand(0, 10000);
        $publicName = uniqid();
        $profilePicture = uniqid();
        $role = \Lib\Data\Profile::ROLE_USER;

        $profile = new \Lib\Data\Profile(
            $id,
            $userId,
            $publicName,
            $profilePicture,
            $role
        );

        $this->assertEquals($id, $profile->getId());
        $this->assertEquals($userId, $profile->getUserId());
        $this->assertEquals($publicName, $profile->getPublicName());
        $this->assertEquals($profilePicture, $profile->getProfilePicture());
        $this->assertEquals($role, $profile->getRole());
    }
}