<?php

/**
 * Class AnswerTest
 */
final class AnswerTest extends \PHPUnit\Framework\TestCase
{
    /**
     *
     */
    public function test_getters_and_setters()
    {
        $id = rand(0, 10000);
        $questionId = rand(0, 10000);
        $position = rand(0, 10000);
        $answerVal = uniqid();
        $isCorrect = boolval(rand(0, 1));

        $answer = new \Lib\Data\Answer(
            $id,
            $questionId,
            $position,
            $answerVal,
            $isCorrect
        );

        $this->assertEquals($id, $answer->getId());
        $this->assertEquals($questionId, $answer->getQuestionId());
        $this->assertEquals($position, $answer->getPosition());
        $this->assertEquals($answerVal, $answer->getAnswer());
        $this->assertEquals($isCorrect, $answer->isCorrect());
    }
}