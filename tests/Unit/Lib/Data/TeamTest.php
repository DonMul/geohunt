<?php

/**
 * Class TeamTest
 */
final class TeamTest extends \PHPUnit\Framework\TestCase
{
    /**
     *
     */
    public function test_getters_and_setters()
    {
        $id = rand(0, 10000);
        $name = uniqid();
        $code = uniqid();
        $points = rand(0, 10000);
        $lat = rand(-180, 180);
        $lon = rand(-90, 90);
        $gameId = rand(0, 10000);

        $team = new \Lib\Data\Team(
            $id,
            $name,
            $code,
            $points,
            $lat,
            $lon,
            $gameId
        );

        $this->assertEquals($id, $team->getId());
        $this->assertEquals($name, $team->getName());
        $this->assertEquals($code, $team->getCode());
        $this->assertEquals($points, $team->getPoints());
        $this->assertEquals($lat, $team->getLat());
        $this->assertEquals($lon, $team->getLon());
        $this->assertEquals($gameId, $team->getGameId());
    }
}