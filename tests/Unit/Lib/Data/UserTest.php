<?php

/**
 * Class UserTest
 */
final class UserTest extends \PHPUnit\Framework\TestCase
{
    /**
     *
     */
    public function test_getters_and_setters()
    {
        $id = rand(0, 10000);
        $username = uniqid();
        $password = uniqid();
        $emailAddress = uniqid() . '@' . uniqid() . '.com';
        $role = \Lib\Data\User::ROLE_USER;

        $user = new \Lib\Data\User(
            $id,
            $username,
            $password,
            $emailAddress,
            $role
        );

        $this->assertEquals($id, $user->getId());
        $this->assertEquals($username, $user->getUsername());
        $this->assertEquals($password, $user->getPassword());
        $this->assertEquals($emailAddress, $user->getEmailAddress());
        $this->assertEquals($role, $user->getRole());
    }
}