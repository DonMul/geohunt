<?php

/**
 * Class BuyableObjectTest
 */
final class BuyableObjectTest extends \PHPUnit\Framework\TestCase
{
    /**
     * 
     */
    public function test_getters_and_setters()
    {
        $id = rand(0, 10000);
        $name = uniqid();
        $image = uniqid();
        $purchaseValue = rand(0, 10000);
        $points = rand(0, 10000);
        $gameId = rand(0, 10000);

        $object = new \Lib\Data\BuyableObject(
            $id,
            $name,
            $image,
            $purchaseValue,
            $points,
            $gameId
        );

        $this->assertEquals($id, $object->getId());
        $this->assertEquals($name, $object->getName());
        $this->assertEquals($image, $object->getImage());
        $this->assertEquals($purchaseValue, $object->getPurchaseValue());
        $this->assertEquals($points, $object->getPoints());
        $this->assertEquals($gameId, $object->getGameId());
    }
}