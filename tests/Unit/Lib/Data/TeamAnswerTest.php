<?php

/**
 * Class TeamAnswerTest
 */
final class TeamAnswerTest extends \PHPUnit\Framework\TestCase
{
    /**
     *
     */
    public function test_getters_and_setters()
    {
        $id = rand(0, 10000);
        $teamId = rand(0, 10000);
        $questionId = rand(0, 10000);
        $answer = uniqid();
        $answerDate = date('Y-m-d H:i:s');
        $isHandled = boolval(rand(0, 1));
        $isCorrect = boolval(rand(0, 1));

        $teamAnswer = new \Lib\Data\TeamAnswer(
            $id,
            $teamId,
            $questionId,
            $answer,
            $answerDate,
            $isHandled,
            $isCorrect
        );

        $this->assertEquals($id, $teamAnswer->getId());
        $this->assertEquals($teamId, $teamAnswer->getTeamId());
        $this->assertEquals($questionId, $teamAnswer->getQuestionId());
        $this->assertEquals($answer, $teamAnswer->getAnswer());
        $this->assertEquals($answerDate, $teamAnswer->getAnswerDate());
        $this->assertEquals($isHandled, $teamAnswer->isHandled());
        $this->assertEquals($isCorrect, $teamAnswer->isCorrect());
    }
}