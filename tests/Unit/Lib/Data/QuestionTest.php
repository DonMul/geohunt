<?php

/**
 * Class QuestionTest
 */
final class QuestionTest extends \PHPUnit\Framework\TestCase
{
    /**
     *
     */
    public function test_getters_and_setters()
    {
        $id = rand(0, 10000);
        $title = uniqid();
        $description = uniqid();
        $value = rand(0, 10000);
        $lat = rand(-180, 180);
        $lon = rand(-90, 90);
        $type = \Lib\Data\Question::TYPE_TEXT;
        $gameId = rand(0, 10000);

        $question = new \Lib\Data\Question(
            $id,
            $title,
            $description,
            $value,
            $lat,
            $lon,
            $type,
            $gameId
        );

        $this->assertEquals($id, $question->getId());
        $this->assertEquals($title, $question->getTitle());
        $this->assertEquals($description, $question->getDescription());
        $this->assertEquals($value, $question->getValue());
        $this->assertEquals($lat, $question->getLat());
        $this->assertEquals($lon, $question->getLon());
        $this->assertEquals($type, $question->getType());
        $this->assertEquals($gameId, $question->getGameId());
    }
}