<?php

/**
 * Class SettingsTest
 */
final class SettingsTest extends \PHPUnit\Framework\TestCase
{
    public function test_get()
    {
        $this->assertEquals(
            'testVal',
            \Lib\Core\Settings::getInstance()->get('test')
        );

        $this->assertEquals(
            ['nested' => [1,2]],
            \Lib\Core\Settings::getInstance()->get('testNested')
        );

        $this->assertEquals(
            [1,2],
            \Lib\Core\Settings::getInstance()->get(['testNested', 'nested'])
        );

        $this->assertNull(
            \Lib\Core\Settings::getInstance()->get(['testOverrride', 'nested'])
        );
    }

    public function test_overrideSettings()
    {
        $uniq = uniqid();
        \Lib\Core\Settings::getInstance()->overrideSettings(['testOverride' => $uniq]);

        $this->assertEquals(
            $uniq,
            \Lib\Core\Settings::getInstance()->get('testOverride')
        );
    }

    public function test_set()
    {
        $uniq = uniqid();
        \Lib\Core\Settings::getInstance()->set('testSet', $uniq);

        $this->assertEquals(
            $uniq,
            \Lib\Core\Settings::getInstance()->get('testSet')
        );
    }

    public function test_getAll()
    {
        $this->assertInternalType(
            'array',
            \Lib\Core\Settings::getInstance()->getAll()
        );
    }
}