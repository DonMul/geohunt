<?php

/**
 * Class TranslationTest
 */
final class TranslationTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var string
     */
    private $originalLanguage;

    /**
     *
     */
    public function setUp()
    {
        $this->originalLanguage = \Lib\Core\Settings::getInstance()->get('defaultLanguage');
    }

    /**
     *
     */
    public function test_getLanguage_retrieved_via_settings()
    {
        Lib\Core\Translation::getInstance()->setLanguage('');

        $newLanguage = 'en';
        \Lib\Core\Settings::getInstance()->set('defaultLanguage', $newLanguage);

        $this->assertEquals(
            $newLanguage,
            \Lib\Core\Translation::getInstance()->getLanguage()
        );
    }

    /**
     *
     */
    public function test_getLanguage_retrieved_via_cookie()
    {
        Lib\Core\Translation::getInstance()->setLanguage('');

        $newLanguage = 'en';
        \Lib\Core\Settings::getInstance()->set('defaultLanguage', uniqid());
        $_COOKIE['language'] = $newLanguage;

        $this->assertEquals(
            $newLanguage,
            \Lib\Core\Translation::getInstance()->getLanguage()
        );
    }

    /**
     *
     */
    public function test_getLanguage_retrieved_via_get_param()
    {
        Lib\Core\Translation::getInstance()->setLanguage('');

        $newLanguage = 'en';
        \Lib\Core\Settings::getInstance()->set('defaultLanguage', uniqid());
        $_COOKIE['language'] = uniqid();
        $_GET['language'] = $newLanguage;

        $this->assertEquals(
            $newLanguage,
            \Lib\Core\Translation::getInstance()->getLanguage()
        );
    }

    /**
     *
     */
    public function test_translate_without_replacements()
    {
        Lib\Core\Translation::getInstance()->setLanguage('en');

        $uniqString = uniqid();
        $this->assertEquals(
            "Test without replacements",
            \Lib\Core\Translation::getInstance()->translate('test.withoutReplacements', ['replacement' => $uniqString])
        );

        $uniqString = uniqid();
        $this->assertEquals(
            "Test without replacements",
            \Lib\Core\Translation::getInstance()->translate('test.withoutReplacements', ['replacement' => $uniqString])
        );
    }

    /**
     *
     */
    public function test_translate_with_replacements()
    {
        Lib\Core\Translation::getInstance()->setLanguage('en');

        $uniqString = uniqid();
        $this->assertEquals(
            "Test with replacements: {$uniqString}",
            \Lib\Core\Translation::getInstance()->translate('test.withReplacements', ['replacement' => $uniqString])
        );

        $uniqString = uniqid();
        $this->assertEquals(
            "Test with replacements: {$uniqString}",
            \Lib\Core\Translation::getInstance()->translate('test.withReplacements', ['replacement' => $uniqString])
        );
    }

    /**
     *
     */
    public function test_getAllLanguages()
    {
        $this->assertEquals(
            ['en', 'nl'],
            \Lib\Core\Translation::getInstance()->getAllLanguages()
        );
    }

    /**
     *
     */
    public function test_translateLink_without_replacements()
    {
        Lib\Core\Translation::getInstance()->setLanguage('en');
        \Lib\Core\Sitemap::getInstance()->getDataForUrl('/');

        $this->assertEquals(
            '/',
            \Lib\Core\Translation::getInstance()->translateLink('index')
        );

        $this->assertEquals(
            '/admin',
            \Lib\Core\Translation::getInstance()->translateLink('admin')
        );
    }

    /**
     *
     */
    public function test_translateLink_with_replacements()
    {
        Lib\Core\Translation::getInstance()->setLanguage('en');
        \Lib\Core\Sitemap::getInstance()->getDataForUrl('/');

        $uniqString = uniqid();
        $this->assertEquals(
            "/cp/game/{$uniqString}/teams",
            \Lib\Core\Translation::getInstance()->translateLink('cpGameTeams', ['gameId' => $uniqString])
        );

        $uniqString = uniqid();
        $this->assertEquals(
            "/cp/game/{$uniqString}/teams",
            \Lib\Core\Translation::getInstance()->translateLink('cpGameTeams', ['gameId' => $uniqString])
        );
    }

    /**
     *
     */
    public function tearDown()
    {
        \Lib\Core\Settings::getInstance()->set('defaultLanguage', $this->originalLanguage);
    }
}