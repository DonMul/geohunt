<?php

/**
 * Class SitemapTest
 */
final class SitemapTest extends \PHPUnit\Framework\TestCase
{
    public function test_getDataForUrl_without_replacements()
    {
        $sitemap = \Lib\Core\Sitemap::getInstance();

        $this->assertEquals(
            [
                'controller' => '\Controller\Index',
                'hash' => 'index',
            ],
            $sitemap->getDataForUrl('/')
        );
    }

    public function test_getDataForUrl_with_replacements()
    {
        $sitemap = \Lib\Core\Sitemap::getInstance();

        $uniqString = uniqid();
        $this->assertEquals(
            [
                'controller' => '\Controller\ControlPanel\Game\Teams',
                'hash' => 'cpGameTeams',
                'params' => [
                    'gameId' => $uniqString,
                ]
            ],
            $sitemap->getDataForUrl("/cp/game/{$uniqString}/teams")
        );
    }

    /**
     *
     */
    public function test_getSitemaps()
    {
        $this->assertEquals(
            ['en', 'nl'],
            array_keys(\Lib\Core\Sitemap::getInstance()->getSitemaps())
        );
    }
}