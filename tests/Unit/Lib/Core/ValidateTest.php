<?php

/**
 * Class ValidateTest
 */
final class ValidateTest extends \PHPUnit\Framework\TestCase
{
    /**
     *
     */
    public function test_notEmpty_true()
    {
        $this->assertTrue(
            \Lib\Core\Validate::getInstance()->notEmpty('This is not empty')
        );
    }

    /**
     *
     */
    public function test_notEmpty_false()
    {
        $this->assertFalse(
            \Lib\Core\Validate::getInstance()->notEmpty('')
        );
    }

    /**
     *
     */
    public function test_isEmpty_true()
    {
        $this->assertTrue(
            \Lib\Core\Validate::getInstance()->isEmpty('')
        );
    }

    /**
     *
     */
    public function test_isEmpty_false()
    {
        $this->assertFalse(
            \Lib\Core\Validate::getInstance()->isEmpty('This is not empty')
        );
    }

    /**
     *
     */
    public function test_isMinLength_true()
    {
        $this->assertTrue(
            \Lib\Core\Validate::getInstance()->isMinLength('MIN_LENGTH', 9)
        );

        $this->assertTrue(
            \Lib\Core\Validate::getInstance()->isMinLength('MIN_LENGTH', 10)
        );
    }

    /**
     *
     */
    public function test_isMinLength_false()
    {
        $this->assertFalse(
            \Lib\Core\Validate::getInstance()->isMinLength('MIN_LENGTH', 11)
        );

        $this->assertFalse(
            \Lib\Core\Validate::getInstance()->isMinLength('MIN_LENGTH', 255)
        );
    }

    /**
     *
     */
    public function test_isMaxLength_true()
    {
        $this->assertTrue(
            \Lib\Core\Validate::getInstance()->isMaxLength('MIN_LENGTH', 11)
        );

        $this->assertTrue(
            \Lib\Core\Validate::getInstance()->isMaxLength('MIN_LENGTH', 255)
        );
    }

    /**
     *
     */
    public function test_isMaxLength_false()
    {
        $this->assertFalse(
            \Lib\Core\Validate::getInstance()->isMaxLength('MIN_LENGTH', 8)
        );

        $this->assertFalse(
            \Lib\Core\Validate::getInstance()->isMaxLength('MIN_LENGTH', 9)
        );
    }

    /**
     *
     */
    public function test_isValidPassword_true()
    {
        $this->assertTrue(
            \Lib\Core\Validate::getInstance()->isValidPassword('3h^Fy!A6Obie5*Xsc^TW')
        );

        $this->assertTrue(
            \Lib\Core\Validate::getInstance()->isValidPassword('8!QsLIb8DcnxX&3gVJLW')
        );
    }

    /**
     *
     */
    public function test_isValidPassword_false()
    {
        // Missing lowercase Chars
        $this->assertFalse(
            \Lib\Core\Validate::getInstance()->isValidPassword('A1B2C3D4E5')
        );

        // Missing uppercase Chars
        $this->assertFalse(
            \Lib\Core\Validate::getInstance()->isValidPassword('a1b2c3d4e5')
        );

        // Missing numbers
        $this->assertFalse(
            \Lib\Core\Validate::getInstance()->isValidPassword('AaBbCcDdEe')
        );

        // Not enough length
        $this->assertFalse(
            \Lib\Core\Validate::getInstance()->isValidPassword('Aa1Bb2')
        );
    }

    /**
     *
     */
    public function test_isValidEmailAddress_true()
    {
        $this->assertTrue(
            \Lib\Core\Validate::getInstance()->isValidEmailAddress('test@example.com')
        );
    }

    /**
     *
     */
    public function test_isValidEmailAddress_false()
    {
        // Missing TLD
        $this->assertFalse(
            \Lib\Core\Validate::getInstance()->isValidEmailAddress('test@example')
        );

        // Missing domain
        $this->assertFalse(
            \Lib\Core\Validate::getInstance()->isValidEmailAddress('test@')
        );

        // Missing localpart
        $this->assertFalse(
            \Lib\Core\Validate::getInstance()->isValidEmailAddress('example.com')
        );
    }

    /**
     *
     */
    public function test_isMinValue_true()
    {
        $this->assertTrue(
            \Lib\Core\Validate::getInstance()->isMinValue(10, 9)
        );

        $this->assertTrue(
            \Lib\Core\Validate::getInstance()->isMinValue(100, 10)
        );
    }

    /**
     *
     */
    public function test_isMinValue_false()
    {
        $this->assertFalse(
            \Lib\Core\Validate::getInstance()->isMinValue(9, 10)
        );

        $this->assertFalse(
            \Lib\Core\Validate::getInstance()->isMinValue(10, 100)
        );
    }

    /**
     *
     */
    public function test_isMaxValue_true()
    {
        $this->assertTrue(
            \Lib\Core\Validate::getInstance()->isMaxValue(9, 10)
        );

        $this->assertTrue(
            \Lib\Core\Validate::getInstance()->isMaxValue(10, 100)
        );
    }

    /**
     *
     */
    public function test_isMaxValue_false()
    {
        $this->assertFalse(
            \Lib\Core\Validate::getInstance()->isMaxValue(10, 9)
        );

        $this->assertFalse(
            \Lib\Core\Validate::getInstance()->isMaxValue(100, 10)
        );
    }
}