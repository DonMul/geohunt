<?php

/**
 * Class SessionTest
 */
class SessionTest extends \PHPUnit\Framework\TestCase
{
    /**
     *
     */
    public function test_isLoggedIn_false()
    {
        $this->assertFalse(
            \Lib\Core\Session::getInstance()->isLoggedIn()
        );
    }

    /**
     *
     */
    public function test_isLoggedIn_true()
    {
        $userKey = uniqid();
        Lib\Core\Session::getInstance()->logIn($userKey);
        $this->assertTrue(
            \Lib\Core\Session::getInstance()->isLoggedIn()
        );

        $this->assertEquals(
            $userKey,
            \Lib\Core\Session::getInstance()->getKey()
        );

        Lib\Core\Session::getInstance()->logOut();
    }

    /**
     *
     */
    public function test_set_and_get()
    {
        $key = uniqid();
        $value = uniqid();

        Lib\Core\Session::getInstance()->set($key, $value);

        $this->assertEquals(
            $value,
            Lib\Core\Session::getInstance()->get($key)
        );
    }
}