<?php

/**
 * Class UtilTest
 */
final class UtilTest extends \PHPUnit\Framework\TestCase
{
    public function test_arrayGet()
    {
        $array = [
            'foo' => [
                'bar' => uniqid()
            ],
        ];

        $this->assertEquals($array['foo'], \Lib\Core\Util::arrayGet($array, 'foo'));
        $this->assertEquals($array['foo'], \Lib\Core\Util::arrayGet($array, ['foo']));
        $this->assertEquals($array['foo']['bar'], \Lib\Core\Util::arrayGet($array, ['foo', 'bar']));

        $uniq = uniqid();
        $this->assertEquals($uniq, \Lib\Core\Util::arrayGet($array, ['foo', 'NOT SET'], $uniq));
        $this->assertNull(\Lib\Core\Util::arrayGet($array, ['foo', 'NOT SET']));
    }
}