<?php

define('RUNNING_IN_TEST', true);
require_once dirname(__FILE__) . '/../private/bootstrap.php';

\Lib\Core\Settings::getInstance()->overrideSettings([
    'defaultLanguage' => 'en',
    'test' => 'testVal',
    'testNested' => [
        'nested' => [
            1,
            2
        ],
    ],
]);