(function ($) {
    $.fn.geoHuntOverview = function(){
        var self = this;

        self.commands = {
            ERROR: 'ERROR',
            PING: 'PING',
            AUTH: 'AUTH',
            PURCHASE: 'PURCHASE',
            ANSWER: 'ANSWER'
        };

        self.fromProjection = new OpenLayers.Projection("EPSG:4326");
        self.toProjection = new OpenLayers.Projection("EPSG:900913");

        self.markers = new OpenLayers.Layer.Vector("Layer 1", {
            styleMap: new OpenLayers.StyleMap({
                'default': OpenLayers.Util.applyDefaults(
                    {label: "${l}", pointRadius: 10},
                    OpenLayers.Feature.Vector.style["default"]
                ),
                'select': OpenLayers.Util.applyDefaults(
                    {pointRadius: 10},
                    OpenLayers.Feature.Vector.style.select
                )
            })
        });

        self.mapnik = new OpenLayers.Layer.OSM(
            "OpenStreetMap",
            [
                '//a.tile.openstreetmap.org/${z}/${x}/${y}.png',
                '//b.tile.openstreetmap.org/${z}/${x}/${y}.png',
                '//c.tile.openstreetmap.org/${z}/${x}/${y}.png'
            ],
            null);

        self.map = new OpenLayers.Map({
            'div': "geoHuntMap",
            'layers': [
                self.mapnik, self.markers
            ]
        });

        self.migrate = function(server) {
            if (server == null) {
                alert("Server is dead, can't continue");
            } else {
                self.uri = server;
                self.init();
            }
        };

        self.init = function() {
            try {
                self.ensureGeoPermissions(function() {
                    if (navigator.geolocation) {
                        self.getPosition(function (position) {
                            var pos = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude
                            };

                            var posit = new OpenLayers.LonLat(pos.lng, pos.lat).transform(self.fromProjection, self.toProjection);
                            var zoom = 18;

                            self.map.setCenter(posit, zoom);

                            var geometry = new OpenLayers.Geometry.Point(pos.lng, pos.lat);
                            geometry.transform(
                                new OpenLayers.Projection("EPSG:4326"),
                                new OpenLayers.Projection("EPSG:900913")
                            );

                            var vector =  new OpenLayers.Feature.Vector(geometry, {
                                l: 'Y'
                            }, OpenLayers.Util.extend(OpenLayers.Feature.Vector.style['default'],  {
                                fillColor: '#000000',
                                fillOpacity: 1,
                                pointRadius: 12
                            }));

                            self.markers.addFeatures([vector]);
                        });
                    } else {
                        self.showMessage("In order to run this application you need to enable GeoLocation");
                        self.error("navigator.geolocation not supported");
                    }

                    setTimeout(self.render, 1000);
                });
            } catch (err) {
                self.error(err);
            }
        };

        self.ensureGeoPermissions = function(callback) {
            navigator.permissions.query({name: 'geolocation'}).then(function (result) {
                if (result.state == 'granted' || result.state == 'prompt') {
                    self.log('GEO PERMISSION', result.state);
                    callback();
                } else if (result.state == 'denied') {
                    self.error("In order to make this game work, you need to allow the site access to your geolocation.")
                }
                result.onchange = function () {
                    self.log('GEO PERMISSION', result.state);
                    callback();
                }
            });
        };

        self.getPosition = function(callback) {
            navigator.geolocation.getCurrentPosition(callback, function(){
                self.error("Something went wrong while fetching GeoLocation")
            }, {
                'enableHighAccuracy': true
            });
        };

        self.render = function() {
            self.getPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                $.post('/services/cp/game/team-locations/', {
                    'gameId': $('.js-game-data').attr('data-game-id')
                }, function(data){
                    self.markers.removeFeatures(self.markers.features);

                    var geometry = new OpenLayers.Geometry.Point(pos.lng, pos.lat);
                    geometry.transform(
                        new OpenLayers.Projection("EPSG:4326"),
                        new OpenLayers.Projection("EPSG:900913")
                    );

                    var vector =  new OpenLayers.Feature.Vector(geometry, {
                        l: 'Y'
                    }, OpenLayers.Util.extend(OpenLayers.Feature.Vector.style['default'],  {
                        fillColor: '#000000',
                        fillOpacity: 1,
                        pointRadius: 12
                    }));

                    self.markers.addFeatures([vector]);

                    var result = JSON.parse(data);
                    $('.team-legend__color').each(function(colorElement){
                        var teamData = result['response']['locations'][$(this).attr('data-team-id')];
                        if (teamData === undefined) {
                            return;
                        }

                        var geometry = new OpenLayers.Geometry.Point(teamData.lon, teamData.lat);
                        geometry.transform(
                            new OpenLayers.Projection("EPSG:4326"),
                            new OpenLayers.Projection("EPSG:900913")
                        );

                        var vector =  new OpenLayers.Feature.Vector(geometry, {
                            l: 'T'
                        }, OpenLayers.Util.extend(OpenLayers.Feature.Vector.style['default'],  {
                            fillColor: $(this).css('background-color'),
                            fillOpacity: 1,
                            pointRadius: 12
                        }));

                        self.markers.addFeatures([vector]);
                    });
                });
            });

            setTimeout(self.render, 1000);
        };

        self.log = function(tag, text) {
            console.log("[" + tag + "] " + text);
        };

        self.error = function(errorMsg) {
            self.log('ERROR', errorMsg);
            self.sendCommand(self.commands.ERROR, {'error': errorMsg});
        };

        self.showMessage = function(message) {
            self.log('MESSAGE', message);
            alert(message);
        };

        self.init();

        window.onbeforeunload = self.close
    };
})(jQuery);