(function ($) {
    $.fn.geohuntGame = function(teamCode){
        var self = this;

        self.teamCode = teamCode;

        self.commands = {
            ERROR: 'ERROR',
            PING: 'PING',
            AUTH: 'AUTH',
            PURCHASE: 'PURCHASE',
            ANSWER: 'ANSWER'
        };

        self.uri = 'geohunt.jmul.net:8080';
        self.questions = JSON.parse($('#questions').attr('data-questions'));
        self.fromProjection = new OpenLayers.Projection("EPSG:4326");   // Transform from WGS 1984
        self.toProjection = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection

        self.layer1 = new OpenLayers.Layer.Vector("Layer 1", {
            styleMap: new OpenLayers.StyleMap({
                'default': OpenLayers.Util.applyDefaults(
                    {label: "${l}", pointRadius: 10},
                    OpenLayers.Feature.Vector.style["default"]
                ),
                'select': OpenLayers.Util.applyDefaults(
                    {pointRadius: 10},
                    OpenLayers.Feature.Vector.style.select
                )
            })
        });

        self.mapnik = new OpenLayers.Layer.OSM(
            "OpenStreetMap",
            [
                '//a.tile.openstreetmap.org/${z}/${x}/${y}.png',
                '//b.tile.openstreetmap.org/${z}/${x}/${y}.png',
                '//c.tile.openstreetmap.org/${z}/${x}/${y}.png'
            ],
            null);

        self.mainMarker = null;
        self.markers = new OpenLayers.Layer.Markers("Markers");

        self.map = new OpenLayers.Map({
            'div': "geoHuntMap",
            eventListeners: {
                featureover: function(e) {
                    e.feature.renderIntent = "select";
                    e.feature.layer.drawFeature(e.feature);
                },
                featureout: function(e) {
                    e.feature.renderIntent = "default";
                    e.feature.layer.drawFeature(e.feature);
                },
                featureclick: function(e) {
                    self.onFeatureClick(e);
                }
            },
            'layers': [
                self.mapnik, self.markers, self.layer1,
            ]
        });

        self.migrate = function(server) {
            if (server == null) {
                alert("Server is dead, can't continue");
            } else {
                self.uri = server;
                self.init();
            }
        };

        self.init = function() {
            self.conn = new WebSocket('wss://' + self.uri);
            self.conn.onclose = function(e) {
                setTimeout(self.init, 1000);
            };

            self.conn.onmessage = function(event) {
                var message = JSON.parse(event['data']);

                self.log('REC', event['data']);

                switch (message['command']) {
                    case 'UPDATE':
                        self.onUpdate(message['payload']);
                        break;
                    case 'MIGRATE':
                        self.migrate(message['payload']['server']);
                        break;
                    case 'MESSAGE':
                        self.showMessage(message['payload']['message']);
                        break;
                    case 'ERROR':
                        self.showMessage(message['payload']['message']);
                        break;
                }
            };

            self.conn.onopen = function() {
                self.sendCommand(self.commands.AUTH, {
                    'teamCode': self.teamCode
                });

                self.log('CONN', "Established");

                try {
                    self.ensureGeoPermissions(function() {
                        self.initializeQuestions();

                        if (navigator.geolocation) {
                            self.getPosition(function (position) {
                                var pos = {
                                    lat: position.coords.latitude,
                                    lng: position.coords.longitude
                                };

                                var posit = new OpenLayers.LonLat(pos.lng, pos.lat).transform(self.fromProjection, self.toProjection);
                                var zoom = 18;

                                self.map.setCenter(posit, zoom);
                                self.mainMarker = new OpenLayers.Marker(posit);
                                self.markers.addMarker(self.mainMarker);
                            });
                        } else {
                            self.showMessage("In order to run this application you need to enable GeoLocation");
                            self.error("navigator.geolocation not supported");
                        }

                        setTimeout(self.render, 1000);
                    });
                } catch (err) {
                    self.error(err);
                }

            };
        };

        self.initModal = function() {
            $('#modalForm').submit(function(event){
                event.preventDefault();
                event.stopPropagation();

                var formArray = $(this).serializeArray();

                var data = {};
                for (var i = 0; i < formArray.length; i++){
                    data[formArray[i]['name']] = formArray[i]['value'];
                }

                self.sendCommand(self.commands.ANSWER, data);
                $('#questionModal').modal('hide');
            });
        };

        self.ensureGeoPermissions = function(callback) {
            navigator.permissions.query({name: 'geolocation'}).then(function (result) {
                if (result.state == 'granted' || result.state == 'prompt') {
                    self.log('GEO PERMISSION', result.state);
                    callback();
                } else if (result.state == 'denied') {
                    self.error("In order to make this game work, you need to allow the site access to your geolocation.")
                }
                result.onchange = function () {
                    self.log('GEO PERMISSION', result.state);
                    callback();
                }
            });
        };

        self.initializeQuestions = function() {
            $(self.questions).each(self.renderQuestion);
        };

        self.getPosition = function(callback) {
            navigator.geolocation.getCurrentPosition(callback, function(){
                self.error("Something went wrong while fetching GeoLocation")
            }, {
                'enableHighAccuracy': true
            });
        };

        self.renderQuestion = function(id,question) {
            var geometry = new OpenLayers.Geometry.Point(question['lon'], question['lat']);
            geometry.transform(
                new OpenLayers.Projection("EPSG:4326"),
                new OpenLayers.Projection("EPSG:900913")
            );

            var vector =  new OpenLayers.Feature.Vector(geometry, {
                l:question['type'] == 'multiple' ? 'M' : question['type'] == 'text' ? 'O' : 'D',
                i:question['id'],
                h:question['title'],
                d:question['description'],
                t:question['type'],
                a:question['answers'],
                lo:question['lon'],
                la:question['lat']
            });

            self.layer1.addFeatures([
                vector
            ])
        };

        self.ping = function(){
            self.getPosition(function (position) {
                self.sendCommand(self.commands.PING, {
                    'lat': position.coords.latitude,
                    'lon': position.coords.longitude,
                });
            });

            setTimeout(self.ping, 5000);
        };

        self.onUpdate = function(result) {
            if (result['objects'] != undefined) {
                for (var x in result['objects']) {
                    var object = result['objects'][x];
                    var objectSpan = $('.js-object-counter[data-object-id="' + object['id'] + '"]');
                    objectSpan.text(object['amount'] || 0);
                }
            }

            if (result['points'] != undefined) {
                var newPoints = result['points'];
                $('.js-points-amount').html(
                    newPoints
                );

                $('.js-purchase-button').each(function(){
                    if (parseInt($(this).attr('data-purchase-value')) <= newPoints) {
                        $(this).removeAttr('disabled');
                    } else {
                        $(this).attr('disabled', 'disabled');
                    }
                });
            }
        };

        self.render = function() {
            self.getPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                var posit = new OpenLayers.LonLat(pos.lng, pos.lat).transform(self.fromProjection, self.toProjection);
                self.map.layers[1].removeMarker(self.map.layers[1].markers[0]);

                self.mainMarker = new OpenLayers.Marker(posit);
                self.markers.addMarker(self.mainMarker);
                self.map.centerLayerContainer(posit);

                for (var x in self.map.layers[2].features) {
                    var feature = self.map.layers[2].features[x];

                    var lat = feature.attributes.la;
                    var lon = feature.attributes.lo;

                    var radLat1 = pos.lat * 0.0174532925;
                    var radLon1 = pos.lng * 0.0174532925;
                    var radLat2 = lat * 0.0174532925;
                    var radLon2 = lon * 0.0174532925;

                    var R = 6371.01;
                    var distance = Math.acos(Math.sin(radLat1) * Math.sin(radLat2) +
                        Math.cos(radLat1) * Math.cos(radLat2) *
                        Math.cos(radLon1 - radLon2)) * R;


                    var visibility = 'hidden';
                    feature.attributes.l = '';
                    if (distance <= 0.05) {
                        visibility = 'visible';
                        feature.attributes.l = feature.attributes.t == 'multiple' ? 'M' : feature.attributes.t == 'text' ? 'O' : 'D';
                    }

                    $('#' + feature.geometry.id).css({'visibility': visibility});
                }
            });

            setTimeout(self.render, 1000);
        };

        self.onFeatureClick = function(e) {
            var modal = $('#questionModal');
            $(modal).modal('show');
            $(modal).find('.modal-title').text(e.feature.attributes.h);
            $(modal).find('.modal-description').text(e.feature.attributes.d);

            $(modal).find('.modal-answers').html('');
            $(modal).find('.js-question-id').val(e.feature.attributes.i);
            if (e.feature.attributes.t == 'multiple') {
                var i = 0;
                for (var x in e.feature.attributes.a) {
                    var answer = e.feature.attributes.a[x];
                    $(modal).find('.modal-answers').html(
                        $(modal).find('.modal-answers').html() +
                        '<div class="modal-answer">' +
                        '<input class="questionRadio" type="radio" ' + (i == 0 ? 'checked="checked"' : '') + ' name="answer" id="' + answer['id'] + '" value="' + answer['id'] + '">' +
                        '<label class="radioLabel" for="' + answer['id'] + '">' + answer['answer'] + '</label>' +
                        '</div>'
                    );
                    i++;
                }
            } else if (e.feature.attributes.t == 'text') {
                $(modal).find('.modal-answers').html(
                    $(modal).find('.modal-answers').html() +
                    '<div class="modal-answer">' +
                    '<textarea name="answer" class="form-control" rows="10"></textarea>' +
                    '</div>'
                );
            }
        };

        self.initPurchaseButtons = function()
        {
            $('.js-purchase-button').click(function(event){
                event.stopPropagation();
                event.preventDefault();

                self.sendCommand(
                    self.commands.PURCHASE,
                    {'objectId': $(this).attr('data-object-id')}
                );
            });
        }

        self.log = function(tag, text) {
            console.log("[" + tag + "] " + text);
        };

        self.error = function(errorMsg) {
            self.log('ERROR', errorMsg);
            self.sendCommand(self.commands.ERROR, {'error': errorMsg});
        };

        self.showMessage = function(message) {
            self.log('MESSAGE', message);
            alert(message);
        };

        self.sendCommand = function(command, payload) {
            if (self.conn.readyState !== self.conn.OPEN) {
                self.log("Can't send command: " + command + ". Socket is not open");
                return;
            }

            self.conn.send(JSON.stringify(
                {
                    'command': command,
                    'payload': payload
                }
            ));
        };

        self.close = function() {
            self.conn.close();
        };

        self.init();
        self.initModal();
        self.initPurchaseButtons();
        self.ping();

        window.onbeforeunload = self.close
    };
})(jQuery);