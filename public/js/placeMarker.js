(function ($) {
    $.fn.placeMarker = function(){
        var self = this;

        self.fromProjection = new OpenLayers.Projection("EPSG:4326");
        self.toProjection = new OpenLayers.Projection("EPSG:900913");

        self.markers = new OpenLayers.Layer.Vector("Layer 1", {
            styleMap: new OpenLayers.StyleMap({
                'default': OpenLayers.Util.applyDefaults(
                    {label: "${l}", pointRadius: 10},
                    OpenLayers.Feature.Vector.style["default"]
                ),
                'select': OpenLayers.Util.applyDefaults(
                    {pointRadius: 10},
                    OpenLayers.Feature.Vector.style.select
                )
            })
        });

        self.mapnik = new OpenLayers.Layer.OSM(
            "OpenStreetMap",
            [
                '//a.tile.openstreetmap.org/${z}/${x}/${y}.png',
                '//b.tile.openstreetmap.org/${z}/${x}/${y}.png',
                '//c.tile.openstreetmap.org/${z}/${x}/${y}.png'
            ],
            {layers: 'basic'});

        self.map = new OpenLayers.Map({
            'div': "geoHuntMap",
            'layers': [
                self.mapnik, self.markers
            ]
        });

        self.migrate = function(server) {
            if (server == null) {
                alert("Server is dead, can't continue");
            } else {
                self.uri = server;
                self.init();
            }
        };

        self.init = function() {
            try {
                self.ensureGeoPermissions(function() {
                    if (navigator.geolocation) {
                        self.getPosition(function (position) {
                            var pos = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude
                            };

                            var lat = $('#lat').val();
                            var lng = $('#lon').val();

                            var posit = new OpenLayers.LonLat(pos.lng, pos.lat).transform(self.fromProjection, self.toProjection);

                            var zoom = 18;
                            self.map.setCenter(posit, zoom);
                            if (lat != 0 || lng != 0) {
                                var geometry = new OpenLayers.Geometry.Point(lng, lat);
                                geometry.transform(
                                    self.fromProjection,
                                    self.toProjection
                                );

                                var vector =  new OpenLayers.Feature.Vector(geometry, {
                                    l: 'Y'
                                }, OpenLayers.Util.extend(OpenLayers.Feature.Vector.style['default'],  {
                                    fillColor: '#000000',
                                    fillOpacity: 1,
                                    pointRadius: 12
                                }));

                                self.markers.addFeatures([vector]);
                            } else {
                                var geometry = new OpenLayers.Geometry.Point(pos.lng, pos.lat);
                                geometry.transform(
                                    self.fromProjection,
                                    self.toProjection
                                );

                                var vector =  new OpenLayers.Feature.Vector(geometry, {
                                    l: 'Y'
                                }, OpenLayers.Util.extend(OpenLayers.Feature.Vector.style['default'],  {
                                    fillColor: '#000000',
                                    fillOpacity: 1,
                                    pointRadius: 12
                                }));

                                self.markers.addFeatures([vector]);
                            }

                            var control = new self.control();
                            self.map.addControl(control);
                            control.activate();
                        });
                    } else {
                        self.showMessage("In order to run this application you need to enable GeoLocation");
                        self.error("navigator.geolocation not supported");
                    }
                });
            } catch (err) {
                self.error(err);
            }
        };

        self.ensureGeoPermissions = function(callback) {
            navigator.permissions.query({name: 'geolocation'}).then(function (result) {
                if (result.state == 'granted' || result.state == 'prompt') {
                    self.log('GEO PERMISSION', result.state);
                    callback();
                } else if (result.state == 'denied') {
                    self.error("In order to make this game work, you need to allow the site access to your geolocation.")
                }
                result.onchange = function () {
                    self.log('GEO PERMISSION', result.state);
                    callback();
                }
            });
        };

        self.getPosition = function(callback) {
            navigator.geolocation.getCurrentPosition(callback, function(){
                self.error("Something went wrong while fetching GeoLocation")
            });
        };

        self.log = function(tag, text) {
            console.log("[" + tag + "] " + text);
        };

        self.error = function(errorMsg) {
            self.log('ERROR', errorMsg);
            self.sendCommand(self.commands.ERROR, {'error': errorMsg});
        };

        self.showMessage = function(message) {
            self.log('MESSAGE', message);
            alert(message);
        };

        self.init();

        self.control = OpenLayers.Class(OpenLayers.Control, {
            defaultHandlerOptions: {
                'single': true,
                'double': false,
                'pixelTolerance': 0,
                'stopSingle': false,
                'stopDouble': false
            },

            initialize: function(options) {
                this.handlerOptions = OpenLayers.Util.extend(
                    {}, this.defaultHandlerOptions
                );
                OpenLayers.Control.prototype.initialize.apply(
                    this, arguments
                );
                this.handler = new OpenLayers.Handler.Click(
                    this, {
                        'click': this.trigger
                    }, this.handlerOptions
                );
            },

            trigger: function(e) {
                var lonlat = self.map.getLonLatFromLayerPx(e.xy);



                var posit = lonlat.transform(self.toProjection, self.fromProjection);
                $('#lat').val(posit.lat);
                $('#lon').val(posit.lon);

                self.markers.features[0].move(lonlat.transform(self.fromProjection, self.toProjection));
            }

        });
        window.onbeforeunload = self.close
    };
})(jQuery);